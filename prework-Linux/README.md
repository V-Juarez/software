<h1>Prework: Configuración de Entorno de Desarrollo en Linux</h1>

<h3>Enrique Devars</h3>

<h1>Tabla de Contenido</h1>

- [1. Instalación de Linux](#1-instalación-de-linux)
  - [¿Qué es Linux y por qué instalarlo?](#qué-es-linux-y-por-qué-instalarlo)
  - [Instalando Ubuntu en VirtualBox](#instalando-ubuntu-en-virtualbox)
  - [Creando una USB booteable con Ubuntu](#creando-una-usb-booteable-con-ubuntu)
  - [Instalando Ubuntu directamente en nuestro equipo](#instalando-ubuntu-directamente-en-nuestro-equipo)
- [2. Herramientas web y editor de texto](#2-herramientas-web-y-editor-de-texto)
  - [¿Qué es el navegador?](#qué-es-el-navegador)
  - [Instalación del navegador y sus DevTools](#instalación-del-navegador-y-sus-devtools)
  - [Instalando nuestro editor de texto](#instalando-nuestro-editor-de-texto)
  - [Extensiones y personalización de Visual Studio Code](#extensiones-y-personalización-de-visual-studio-code)
  - [Cómo usar Live Server en proyectos reales de HTML y CSS](#cómo-usar-live-server-en-proyectos-reales-de-html-y-css)
- [3. Introducción a la terminal](#3-introducción-a-la-terminal)
  - [Comandos básicos de la terminal](#comandos-básicos-de-la-terminal)
  - [Instalación de Node.js](#instalación-de-nodejs)
  - [Tu primer proyecto con React](#tu-primer-proyecto-con-react)
- [4. Introducción a Git y Github](#4-introducción-a-git-y-github)
  - [¿Qué es Git y Github?](#qué-es-git-y-github)
  - [Configurando nuestras credenciales](#configurando-nuestras-credenciales)
  - [Haciendo nuestros primeros commits](#haciendo-nuestros-primeros-commits)
  - [Subiendo nuestro primer repositorio](#subiendo-nuestro-primer-repositorio)
- [5. Despedida](#5-despedida)
  - [¿Qué es lo que sigue?](#qué-es-lo-que-sigue)


# 1. Instalación de Linux

## ¿Qué es Linux y por qué instalarlo?

**¿Que es Linux?**

Es una famiia de SO(Sistemas Operativos) los cuales tienen algo en comun, ya que todos usan el Kernel de Linux.

**¿Donde esta presente linux?**

En muchos lados como por ejemplo:

- Telefonos
- Servidores
- Terminales de punto de venta
- Distribuciones


Tenemos muchas, cada una con su propio manejador de paquetes y herramientas, entre las mas usadas tenemos:

  - Fedora
  - Arch Linux
  - Ubuntu

**Ubuntu**: Tiene varios flavors, sabores, como quieras llamarle, lo principal es gentil, amigable para nuevos usuarios. Bueno para principiantes.

Prueben Ubuntu Budgie, me ha venido muy bien.

**ArchLinux**: Es para usuarios avanzados, por que tienes que personalizar casi todo. Si tienes tiempo de sobra puedes utilizarlo y hacerlo a tu modo. Es decir escoger el tipo de manejador de interfaz gráfica, escoger el tipo de terminal, etc.

**Fedora**: No lo he usado mucho. Pero pase varios años utilizando su antecesor RedHat cuando era libre era una chulada.
Hay demasiadas versiones, como Slackware, un linux lo más puro por decirlo y muy antiguo (no novatos). CentOS, un linux que trae seguridad incorporada para ambiente servidor. Clear Linux, una distribución desarollada de Intel para trabajar con la nube. Y muchas más.

## Instalando Ubuntu en VirtualBox

Ubuntu es una de tantas distribuciones basadas en Debian que es una distribución madre.

Ubuntu usa por defecto su DE (Desktop Enviroment) basado en GNOME. Hay varias derivaciones de Ubuntu de acuerdo a su DE como Kubuntu, Xubuntu, Lubuntu y muchas… Todas creadas con un proposito especifico.

> Aparte ubuntu también podemos utilizar lo que es xubuntu, lubuntu, kubuntu, linuxlite version más ligeras para pc con componentes limitados.
>
> Recomendación: Traten de hacer una partición en su disco duro e instalen Ubuntu allí. Como dice el profe el rendimiento es mucho mejor.

> Click, en imagen de `Cd Guest aditions`, ejecutar. Instalar driver y modulos para que virtual box, ejecute correctamente ubuntu. Reiniciamos Ubuntu.

[Error Virtual box, and Install Ubuntu Linux](https://www.youtube.com/watch?v=sjnJaJQ59hs)

![img](https://www.google.com/s2/favicons?domain=https://www.virtualbox.org//graphics/VirtualBox.ico)[Oracle VM VirtualBox](https://www.virtualbox.org/)

![img](https://www.google.com/s2/favicons?domain=https://assets.ubuntu.com/v1/49a1a858-favicon-32x32.png)[Enterprise Open Source and Linux | Ubuntu](https://ubuntu.com/)

## Creando una USB booteable con Ubuntu

Para crear una usb booteable directamente desde linux se puede usar el comando `dd`

```bash
dd bs=4M if=/home/gollum23/Downloads/manjaro-kde-21.0.4-210506-linux510.iso of=/dev/sdc status=progress oflag=sync
```

**bs** -> tamaño de lo bloques al leer y escribir
**if** -> ruta absoluta donde se encuentra la imagen que quieres usar
**of** -> ruta absoluta de la memoria usb
**status** -> Muestra estadísticas de transferencia de los datos a la usb
**oflag=sync** -> Mantiene la terminal en el proceso hasta que termina la creación de la usb

Es muy practico de usar y bastante rápido.

Hay mas opciones y configuraciones que puedes usar las cuales encuentras usando el comando `dd --help`

> software para crear USB Booteables es balenaEtcher. Es bueno y corre en Linux nativo a diferencia que Rufus lo hace solo en Windows.

Hay que haces una aclaración para este metodo, si tu computadora no tiene el modo BIOS y solo tiene el modo UEFI en rufus debes igualmente seleccionar system type UEFI, hay computadoras que no soportan el modo predeterminado de rufus.

Otra cosa es que en la bios de tu computadora debes desactivar el secure boot para poder arrancar el usb. Si quieres instalar ubuntu con windows en la misma computadora el proceso es ligeramente diferente y hay que ser bastante cuidadoso, porque si tu computadora tiene secure boot puedes dejar inaccesible windows sin embargo si buscas en google windows, linux dual secure boot encontraras como reparar el dual boot, esto basicamente es sobreescribir el sector de arranque de tu disco duro donde antes se usaba el windows boot manager por el grub uefi que instala ubuntu

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Rufus - Cree unidades USB arrancables fácilmente](https://rufus.ie/es/)

![img](https://www.google.com/s2/favicons?domain=https://assets.ubuntu.com/v1/49a1a858-favicon-32x32.png)[Download Ubuntu Desktop | Download | Ubuntu](https://ubuntu.com/download/desktop)

## Instalando Ubuntu directamente en nuestro equipo

**BIOS** según la marca de tu pc.

<img src="https://i.ibb.co/Vpgm29m/Bios.jpg" alt="Bios" border="0">

> Desactivar Security boot. 
>
> Ubuntu es compatible con secure boot, no es necesario desactivarlo.

# 2. Herramientas web y editor de texto

## ¿Qué es el navegador?

### ¿Que navegador instalar si estas en el mundo del desarrollo web?

La respuesta es TODOS. como desarrollador web tu trabajo será crear aplicaciones que se ejecuten sim importar cual sea el navegador que use el publico por ello deberñas poder probar lo que hagas en todos los navegadores, no hay que pensar en que si se ve bien o si se ejecuta bien en un navegador es suficiente, por eso hay que tenerlos todos o por lo menos la mayoria de ellos (En linux no puedes tener safari que es de mac pero si puedes tener opera que usa el mismo motor, el internet explorer pues hay formas de instalarlo en linux pero no es sencillo)

![img](https://www.google.com/s2/favicons?domain=https://www.google.com/intl/es-419/chrome//chrome/static/images/favicons/favicon-16x16.png)[Navegador web Google Chrome](https://www.google.com/intl/es-419/chrome/)

![img](https://www.google.com/s2/favicons?domain=https://www.google.com/intl/es-419/chrome/dev//chrome/static/images/favicons/favicon-16x16.png)[Chrome Dev](https://www.google.com/intl/es-419/chrome/dev/)

![img](https://www.google.com/s2/favicons?domain=https://www.google.com/intl/es-419/chrome/canary//chrome/static/images/favicons/favicon-16x16.png)[Chrome Canary](https://www.google.com/intl/es-419/chrome/canary/)

## Instalación del navegador y sus DevTools

Para actualizar en sistemas operativos basados en Arch linux el comando para actualizar e instalar actualizaciones es

```bash
sudo pacman -Syyu
```

Si tienen distribusiones basadas en Arch como manjaro pueden usar un manejador de paquetes llamado `yay`

```bash
yay -Syyua
```

Excelentes comandos. 

Un truco para acortar ello y que servirá sería usar &&:

```bash
sudo apt update && sudo apt full-upgrade
```

![img](https://www.google.com/s2/favicons?domain=https://www.google.com/intl/es-419/chrome//chrome/static/images/favicons/favicon-16x16.png)[Navegador web Google Chrome](https://www.google.com/intl/es-419/chrome/)

## Instalando nuestro editor de texto

![img](https://www.google.com/s2/favicons?domain=https://code.visualstudio.com//favicon.ico)[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

## Extensiones y personalización de Visual Studio Code

Otras extensiones útiles:

- indent-rainbow: Colorea la identacion del codigo (colorea los espacios de los tabs)
- Auto Close Tag: Cierra los tags de forma automatica
- Git History: Muestra el historial de git
- GitLens: Tambien muestra el historial de git pero desde el codigo y tiene mas funciones…
- Todo Tree: Con esta extension se puede crear una lista de tareas pendientes desde el mismo editor
- Tabnine AI Code Completion: Una IA que te autocompleta codigo.
- Bookmarks : Les permite agregar un marcador en su código, para tenerlo en cuenta después o guardar algo importante
- CodeSnap : Les permite sacar una captura de un pedazo de su código, así lo comparten muchos desarrolladores en redes sociales porque se ve bien estéticamente y es fácil
- ✨ Elm Emmet ✨ : Autocompletado espectacular para HTML, incluso puede hacer muchas cosas en una línea y al dar tab se crea, es genial mírenla
- ENV : Agregar colores a los archivos .env que se usan para manejar variables de entorno
- html2pug : Si quieren usar el preprocesador Pug y tienen algo en HTML con esta extensión lo pueden convertir de HTML a Pug fácilmente
- Npm Intellisense : Les ayuda a importar librerías que hayan instalado en sus archivos rápidamente
- ✨ Thunder Client ✨ : Les permite hacer peticiones HTTP desde el mismo VSC. Así no necesitarán Postman o Insomia sino que desde el mismo editor puede hacer las peticiones con todo lo necesario y organizarlo en colecciones.
- ✨ Wakatime ✨ : Les cuenta cuánto tiempo programaron en la semana, en que lenguajes y en que proyectos. Se las recomiendo para mirar su productividad ya que cada semana te da un reporte
- ✨ Webpack Snippets ✨ : Cuando aprendan Webpack se darán cuenta que muchas cosas siempre se repiten, esta extensión les ayudará a ser más rápidos en webpack y crear cosas más rápidamente.

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Material Icon Theme - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Prettier - Code formatter - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Color Highlight - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Live Server - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Auto Rename Tag - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)

## Cómo usar Live Server en proyectos reales de HTML y CSS

Color Visual Studio Code:

```css
body{
    background-color: rgb(91, 91, 150);
}
```

color, que permite a los ojos ver la pantalla.

# 3. Introducción a la terminal

## Comandos básicos de la terminal

Listar archivos:

```bash
ls
```

Listar archivos para ver su peso de una manera mas mas legible

```bash
ls -lh
```

Listar archivos ocultos:

```bash
ls -a
```

Identificar la ruta en la que estamos en nuestro sistema:

```bash
pwd
```

Movernos entre directorios:

```bash
cd
```

Crear un directorio:

```bash
mkdir
```

Copiar un archivo:

```shell
cp
```

Borrar un archivo:

```shell
rm
```

Mover un archivo:

```shell
mv
```

Borrar un directorio:

```shell
rmdir
```

Limpiar la terminal

```bash
clear
```

Para borrar un directorio 

```bash
rm -r nombre_de-directorio
```

## Instalación de Node.js

Instalar `nvm`:

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```

Paso recomendable después de instalar nvm y antes de instalar cualquier versión de node es correr en la terminal:

```bash
source ~/.bashrc
```

y ya pueden usar el comando nvm correctamente, ésto es solo para obtener el archvo bashrc que quedó en la carpeta de instalación y que es necesario para iniciar nvm.
Otra cosa más:
si de pronto llegaran a necesitar una versión especifica de node por ejemplo digamos la 15.2.1 se le puede especificar al comando de instalación:

```bash
nvm install v15.2.1
```

para comprobar que versiones hay disponibles: (la lista es larga)

```bash
nvm list-remote
```

si quiero instalar otra versión simplemente ejecuto el mismo comando de instalación con el numero de versión que quiero de la lista con la “v”

también puedo ver que versiones tengo instaladas con:

```bash
nvm list
```

la última versión que instalas de node se convierte en la versión en uso por defecto pero la puedes cambiar especificando cual quieres usar entre las que tienes instaladas:

```bash
nvm use v15.2.1
```

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - nvm-sh/nvm: Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions](https://github.com/nvm-sh/nvm#manual-install)

![img](https://www.google.com/s2/favicons?domain=https://nodejs.org/en/static/images/favicons/apple-touch-icon.png)[Node.js](https://nodejs.org/en)

## Tu primer proyecto con React

El link de las instrucciones para crear una nueva aplicación de React:

[create-a-new-react-app](https://es.reactjs.org/docs/create-a-new-react-app.html)

Script de creacion de proyecto en REact:

```bash
# Creamos nuestro proyecto
npx create-react-app nombre-app

# Nos movemos a la carpeta del proyecto
cd my-app

# Iniciamos React
npm start
```

![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org//favicon.ico)[React – Una biblioteca de JavaScript para construir interfaces de usuario](https://es.reactjs.org/)

# 4. Introducción a Git y Github

## ¿Qué es Git y Github?

**GIT**
Es el sistema de control de versiones, el cual nos permite llevar un control exacto de todos los cambios a lo largo del tiempo de nuestro proyecto, ademas no solo es que guarda un copia Completa del proyecto, solo guarda los cambios importantes que se hagan.

**GITHUB**
Es una plataforma en la cual podemos subir nuestros repositorios de GIT a la nube, para asi poder tener un respaldo y ademas poder compartir lo que hagamos con la comunidad, ya que GitHub es como el Facebook de los programadores. Y tambien poder trabajar en proyectos con distintos colaboradores a la vez.

<img src="https://i.ibb.co/xCfMR47/git.jpg" alt="git" border="0">

**Arch Linux** es con : 

```bash
sudo pacman -S git
```

 Instalar Git en **ubuntu, Debian, Mint**:

```bash
sudo apt install git
```

![img](https://www.google.com/s2/favicons?domain=https://git-scm.com//favicon.ico)[Git](https://git-scm.com/)

## Configurando nuestras credenciales

```bash
# nombre de usuario
git config --global user.name "name-user"

# email
git config --global user.name "email@gmail.com"

# listar configuracion
git config --list

# Opcional 
git config --global init.defaultBranch main

```

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub: Where the world builds software · GitHub](https://github.com/)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Generating a new SSH key and adding it to the ssh-agent - GitHub Docs](https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

## Haciendo nuestros primeros commits

```bash
# Iniciar
git init

# Cargar inicio staying
git add .

# Crear commit
git commit -m "mensaje"

# Enviar los archivos al repositorio remoto
git push origin main <rama>

```



## Subiendo nuestro primer repositorio

```bash
# config Key
ssh-keygen -t rsa -b 4096 -C "email"
```

seguridad usen la encriptación [Ed25519](https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) que recomienda Github. [Encriptación para SSH](https://qastack.mx/ubuntu/363207/what-is-the-difference-between-the-rsa-dsa-and-ecdsa-keys-that-ssh-uses)

```bash
ssh-keygen -t ed25519 -C “your_email@example.com”
```

**Config key private in local**

```bash
# Verificar key
eval "$(ssh-agent -s)"

# Agregar key private
ssh-add ~/.ssh/id_rsa
```

En GitHub, vamos `account settings > SSH and GPG keys > SSH keys` | Agregamos nuestra  `public Key`.

`Instalalr xclip`

```bash
# Instalar utilidad
sudo apt install xclip

# Copiar de manera segura
xclip -selection clipboard < ~/.ssh/id_rsa.pub
```

![img](https://www.google.com/s2/favicons?domain=https://docs.github.com/es/github/authenticating-to-github/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account/assets/images/site/favicon.png)[Agregar una clave SSH nueva a tu cuenta de GitHub - GitHub Docs](https://docs.github.com/es/github/authenticating-to-github/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)

![img](https://www.google.com/s2/favicons?domain=https://docs.github.com/es/github/authenticating-to-github/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/assets/images/site/favicon.png)[Generar una nueva clave SSH y agregarla al ssh-agent - GitHub Docs](https://docs.github.com/es/github/authenticating-to-github/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

![img](https://www.google.com/s2/favicons?domain=https://media.geeksforgeeks.org/wp-content/cdn-uploads/gfg_favicon.png)[Difference between Private key and Public key - GeeksforGeeks](https://www.geeksforgeeks.org/difference-between-private-key-and-public-key/)

# 5. Despedida


## ¿Qué es lo que sigue?

Linux es el mejor S.O