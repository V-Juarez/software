#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
  printf("Variables y tipos de datos \n\n");

  // Declaraion de tipos de datos primitivos
  int enteroA = 1;
  float flotanteA = 1.1;
  double doubleA = 1.2;
  char caracterA = 'S';

  //Mostrar en pantalla las variables, En entrada y salida de datos se explicara la funcion "printf"
  printf("El valor de entero A es: %i \n", enteroA);
  printf("El valor de flotante A es: %f \n", flotanteA);
  printf("El valor de double A es: %f \n", doubleA);
  printf("El valor de caracter A es: %c \n", caracterA);

  return 0;
}