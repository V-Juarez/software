# Conseguir Trabajo en Programación

### Jorge Zavala

- [1. Introducción al curso](#1.-Introducción-al-curso)
  - [¿De qué trata el curso?](#¿De-qué-trata-el-curso?)
- [2. El reto de ser un desarrollador nivel Silicon Valley](#2.-El-reto-de-ser-un-desarrollador-nivel-Silicon-Valley)
  - [¿Qué necesitas para comenzar?](#¿Qué-necesitas-para-comenzar?)
  - [Networking](#Networking)
  - [Minimum Viable Mindset](#Minimum-Viable-Mindset)
- [3. Construyendo tu perfil](#3.-Construyendo-tu-perfil)
  - [Portafolio](#Portafolio)
  - [Entrevista y caso de éxito](#Entrevista-y-caso-de-éxito)
- [4. Oportunidades para crecer en el área de Programación](#4.-Oportunidades-para-crecer-en-el-área-de-Programación)
  - [Conectándote con la comunidad](#Conectándote-con-la-comunidad)
  - [Tips para ser freelance en programación](#Tips-para-ser-freelance-en-programación)
  - [Mejorar como desarrollador](#Mejorar-como-desarrollador)

***

# 1. Introducción al curso

  ## ¿De qué trata el curso?

  [Conseguir Trabajo.pdf](https://drive.google.com/file/d/1-9tt_97HK8COC4O7JBWiziYlYQ9-jk-J/view?usp=sharing)


# 2. El reto de ser un desarrollador nivel Silicon Valley

  ## ¿Qué necesitas para comenzar?

  - Trabajar desde tu lugar de origen
  - Trabajo de alto valor.
  - Haz lo que te gusta.
  - Haz tu propia empresa o se parte de un gran proyecto.

  > “Nos es que estés trabajando, estas **disfrutando** lo que tu construyes”

  ### Como  es la vda de desarrollo en el Silincon Valley?

  * Optimizar el software que tiene todo el tiempo.
  * Constante aprendizaje y actualizaci&oacute;n t&eacute;cnica.
  * **Mucha Competencia!** la gente est&aacute; en el Silicon Valley son los mejores del Mundo.

  ### Qu&eacute; hace diferente al programador del Silicon Valley?

  - Curiosidad
  - Habilidad para resolver problemas
  - Enfoque a resultados
  - Explotar herramientas y formas de hacer las cosas


  ### Informaci&oacute;n

  * Como puedo estar al dia?
      - [TechCrunch](https://techcrunch.com)
      - Reddit
      - [Producthunt](https://www.producthunt.com)

  * Colabora activamente y desarrollar nuevas ideas.

  ## Networking

  - Eventos, meetups, conferencias
  - tendencias y l&iacute;deres de opini&oacute;n
  - Identificar y crear amigos con intereses comunes

  ### D&oacute;nde operar?

  Tu trabajo. hoy en d&iacute;a lo puedes hacer donde quieras en el mundo:

  - En mi localidad
  - Explotar un centro de emprendimiento local
  - Explotar el mundo y desarrollar con una visi&oacute;n global

  ### Oportunidades para crear una carrera como Desarrollador de clase mundial

  **C&oacute;mo crear valor?**
  
  - Crear una experiencia de usuario UX interesante
  - Desarrolla oara kis temas de actualidad

    - VR
    - AI
    - Big Data

  - Crear ambientes de operaci&oacute;n en el back-end

  ## Minimum Viable Mindset

- Pensar en hacer un gran producto que requiere

  - Buen diseno
  - Operaci&oacute;n confiable
  - Eficiente que resuelva en forma simple un problema

  [The Minimun Viable Mindset](https://es.slideshare.net/zavala55/the-minimum-viable-mindset)

    ### Qu&eacute; tan hambriento est&aacute;s?

    - Conversas tus aspiraciones con expertos?
    - Tienes un mentor?
    - C&oacute;mo aprender a hacer software?
    - Valoras tus fracasos como fuente de aprendizaje?

    ### Tu estado de &aacute;

    - Tu Mindset determina tu &eacute;xito.
    - Los retos son grandes, Estas listo?
    
# 3. Construyendo tu perfil

  ## Portafolio

- Portafolio de proyectos.
- Github, Linkedln.
- Lenguajes
- Base de Datos
- M&eacute;todos de trabajo
- Herramientas

  > “Cada proyecto que has hecho es una parte de ti, es algo que quieres exhibir, es algo que quieres mostrar”

  ### Tu historia

Solo tus proyectos te abren puertas y muestran qui&eacute;n eres como programador.

  - Qu&eacute; te apasiona hacer?
  - Hacer vs Mantener
  - experiencias y recomendaciones de la gente con que hemos trabajando
  - El proyecto est&aacute; haciendo
  - Tu proyecto estrella
  - Tu proyecto m&aacute; reconocido

  ## Entrevista y caso de éxito

* Qu&eacute; tipo de entrevista?

  - La experiencia de personas en el Silicon Valley

    - Invitados

  - C&oacute;mo prepararte

    - Ejercicios
    - Amigos
    - Concursos

    > “Ser programador no es simplemente escribir lineas de código, es transformar la vida de las personas que te rodean”

  ### Emocionada: Es hora de Hacerlo!

  - Es tu momento de salier a explorar el mundo
  - Uno gana solo si esta el juego
  - La experiencia de hacer y el aprendizaje que genera

    **[Caso de Exito - Sillicon Valley | Jorge Cuadra ](https://www.youtube.com/watch?v=qQPmM-G2X_E)**

# 4. Oportunidades para crecer en el área de Programación

  ## Conectándote con la comunidad

- **[RedGlobalMxSF](https://www.fb.com/groupsredglobalmxsf/)**
- **[Tieeco](https://tieeco.org/)**
- **[SiliconValleyForum](http://siliconvalleyforum.com/)**
- **[Ecorner](https://ecorner.stanford.edu/)**

  ## Tips para ser freelance en programación

Sitios web para iniciar para ser freelance

  - **[Workana](https://www.workana.com/es)**
  - **[Toptal](https://www.toptal.com/)**
  - **[Freelancer](https://www.freelancer.com/)**
  - **[Upwork](https://www.upwork.com/)**
  - **[Freelance](https://www.freelance.com/)**

  ## Mejorar como desarrollador

Oportunidades de crecimiento:

  - **[Kaggle](https://www.kaggle.com/)**
  - **[Codefights](https://codefights.com/)**
  - **[Topcoder](https://www.topcoder.com/)**
  - **[Codility](https://www.codility.com/)**
  - **[Codecombat](https://codecombat.com/)**
  - **[Battlecode](https://www.battlecode.org/)**
  - **[Hackerrank](https://www.hackerrank.com/)**
  - **[CodeWars](https://www.codewars.com)**
  - **[Spoj](https://www.spoj.com/)**