<h1>Product Management for Developers</h1>

<h3>David Delgado</h3>

<h1>Tabla de Cotenido</h1>

- [1. Presentación](#1-presentación)
  - [Introducción](#introducción)
- [2. El problema fundamental: balance](#2-el-problema-fundamental-balance)
  - [El reto: un Product Manager con fuertes nociones técnicas](#el-reto-un-product-manager-con-fuertes-nociones-técnicas)
- [3. El producto](#3-el-producto)
  - [Agenda de Producto: Herramientas](#agenda-de-producto-herramientas)
  - [La piedra angular de tu empresa](#la-piedra-angular-de-tu-empresa)
  - [Desarrollo del producto](#desarrollo-del-producto)
  - [Agenda de Producto: Herramientas](#agenda-de-producto-herramientas-1)
  - [Proyecto de curso](#proyecto-de-curso)
  - [El Producto de software + Git](#el-producto-de-software--git)
  - [Repaso de Git](#repaso-de-git)
  - [Escalabilidad y productización](#escalabilidad-y-productización)
  - [Innovación y visión](#innovación-y-visión)
  - [Kitchen API - Productización y escalabilidad](#kitchen-api---productización-y-escalabilidad)
- [4. Los stakeholders](#4-los-stakeholders)
  - [Stakeholders externos: clientes](#stakeholders-externos-clientes)
  - [Un CAFE para el cliente](#un-cafe-para-el-cliente)
  - [Guía a tu Cliente](#guía-a-tu-cliente)
  - [Stakeholders Internos - Tu compañia](#stakeholders-internos---tu-compañia)
  - [El equipo de customer](#el-equipo-de-customer)
  - [El equipo de operaciones](#el-equipo-de-operaciones)
  - [El equipo de soporte](#el-equipo-de-soporte)
  - [Cierre Módulo - Caso de Estudio](#cierre-módulo---caso-de-estudio)
- [5. Tu equipo](#5-tu-equipo)
  - [Conociendo a tu equipo](#conociendo-a-tu-equipo)
  - [Proveer un arquetipo de los miembros de un equipo](#proveer-un-arquetipo-de-los-miembros-de-un-equipo)
  - [Creando/uniéndote a una cultura de equipo](#creandouniéndote-a-una-cultura-de-equipo)
  - [La comunicación es la clave](#la-comunicación-es-la-clave)
  - [Dar y recibir feedback](#dar-y-recibir-feedback)
  - [Cómo gestionar a tu equipo](#cómo-gestionar-a-tu-equipo)
  - [Asignar tareas a tu equipo](#asignar-tareas-a-tu-equipo)
  - [Tu rol como líder](#tu-rol-como-líder)
  - [PDP - Plan Desarrollo Profesional](#pdp---plan-desarrollo-profesional)
  - [Ejercicio PDP](#ejercicio-pdp)

# 1. Presentación

## Introducción



[Product-Management-for-Developers.pdf](https://drive.google.com/file/d/1QlMimzKB51LL8yAZ3V5hIAsrAM55RsIc/view?usp=sharing)

# 2. El problema fundamental: balance


## El reto: un Product Manager con fuertes nociones técnicas

# 3. El producto


## Agenda de Producto: Herramientas


## La piedra angular de tu empresa


## Desarrollo del producto


## Agenda de Producto: Herramientas


## Proyecto de curso


## El Producto de software + Git


## Repaso de Git


## Escalabilidad y productización


## Innovación y visión


## Kitchen API - Productización y escalabilidad

# 4. Los stakeholders


## Stakeholders externos: clientes


## Un CAFE para el cliente


## Guía a tu Cliente


## Stakeholders Internos - Tu compañia


## El equipo de customer


## El equipo de operaciones


## El equipo de soporte


## Cierre Módulo - Caso de Estudio

# 5. Tu equipo


## Conociendo a tu equipo


## Proveer un arquetipo de los miembros de un equipo


## Creando/uniéndote a una cultura de equipo


## La comunicación es la clave


## Dar y recibir feedback


## Cómo gestionar a tu equipo


## Asignar tareas a tu equipo

## Tu rol como líder

**Liderazgo**:

- Demócratico: Basado en las deciciones del equipo
- Estrategico: Cambia el estilo de liderazgo según la situación
- Transformacional: Innovador, genera nuevas iniciativas y formas de comunicación
- Entrenador: Es como un equipo de futbol donde se capitalizan las fortalezas del equipo para lograr el objetivo


## PDP - Plan Desarrollo Profesional


## Ejercicio PDP