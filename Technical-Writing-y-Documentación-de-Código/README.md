# Technical Writing y Documentación de Código

#### Nancy Salazar

# Tabla de Contenido
- [1. Introducción al Technical Writing](#1.-Introducción-al-Technical-Writing)
  - [¿Qué es Technical Writing? Lleva tu documentación al siguiente nivel](#Qué-es-Technical-Writing-Lleva-tu-documentación-al-siguiente-nivel)
  - [Habilidades para convertirte en Technical Writer](#Habilidades-para-convertirte-en-Technical-Writer)
  - [¿Conoces a tu público? Escribe específicamente para tu audiencia](#Conoces-a-tu-público-Escribe-específicamente-para-tu-audiencia)
  - [Cómo entrevistar equipos de programación para recolectar información técnica](#Cómo-entrevistar-equipos-de-programación-para-recolectar-información-técnica)
- [2. Estructura gramatical](#2.-Estructura-gramatical)
  - [Un repaso por la gramática básica](#Un-repaso-por-la-gramática-básica)
  - [Voz activa vs. voz pasiva: estándares y estructura de una oración](#Voz-activa-vs-voz-pasiva-estándares-y-estructura-de-una-oración)
  - [Uso correcto de acrónimos y abreviaturas para explicar términos desconocidos](#Uso-correcto-de-acrónimos-y-abreviaturas-para-explicar-términos-desconocidos)
- [3. Técnicas de escritura fundamentales para documentos técnicos](#3.-Técnicas-de-escritura-fundamentales-para-documentos-técnicos)
  - [Sigue las reglas de George Orwell para escribir con claridad](#Sigue-las-reglas-de-George-Orwell-para-escribir-con-claridad)
  - [Uso correcto de listas y tablas para ordenar información](#Uso-correcto-de-listas-y-tablas-para-ordenar-información)
  - [Tipos de párrafos y paso a paso para estructurarlos](#Tipos-de-párrafos-y-paso-a-paso-para-estructurarlos)
- [4. Conceptos básicos de programación e ingeniería de software](#4.-Conceptos-básicos-de-programación-e-ingeniería-de-software)
  - [¿Qué es programación? Evolución de la documentación y technical writing](#¿Qué-es-programación?-Evolución-de-la-documentación-y-technical-writing)
  - [Lenguajes de programación, tipos de datos y estructura de documentos HTML](#Lenguajes-de-programación-tipos-de-datos-y-estructura-de-documentos-HTML)
- [5. Estándares de documentación de código](#5.-Estándares-de-documentación-de-código)
  - [Cómo documentar una función de código](#Cómo-documentar-una-función-de-código)
  - [Buenas prácticas de legibilidad para código y comentarios](#Buenas-prácticas-de-legibilidad-para-código-y-comentarios)
- [6. Organización y revisión de tu documentación](#6.-Organización-y-revisión-de-tu-documentación)
  - [Organiza y define el alcance de tus documentos](#Organiza-y-defin--el-alcance-de-tus-documentos)
  - [Utiliza Markdown en documentos técnicos](#Utiliza-Markdown-en-documentos-técnicos)
  - [Guía para revisar documentación en equipo de manera efectiva](#Guía-para-revisar-documentación-en-equipo-de-manera-efectiva)
  - [Cómo organizar documentos largos](#Cómo-organizar-documentos-largos)
- [7. Diseño de documentos](#7.-Diseño-de-documentos)
  - [Crea ilustraciones instructivas](#Crea-ilustraciones-instructivas)
- [8. Conclusiones](#8.-Conclusiones)
  - [Siguientes pasos para convertirte en Technical Writer profesional](#Siguientes-pasos-para-convertirte-en-Technical-Writer-profesional)

***

# 1. Introducción al Technical Writing

  ## ¿Qué es Technical Writing? Lleva tu documentación al siguiente nivel

  #### Technical Writing: más allá de manuales y documentación 📚

**¡Te doy la bienvenida al Curso de Technical Writing y Documentación de Código con Markdown!** Aprenderemos cómo documentar código siguiendo buenas prácticas para facilitar el mantenimiento y escalabilidad de tus proyectos.

No te desanimes si aún no eres una experta programadora. En realidad, el objetivo del technical writing o escritura técnica es mucho más profundo que solo la creación de manuales para software y computadoras, podemos llevar a cabo esta disciplina en cualquier campo donde necesites explicar ideas, conceptos y procesos complejos. Por esta razón, en este curso quiero compartirte todo lo que involucra trabajar como *Technical Writer*.

  #### ¡Soy Nancy Salazar, tu profesora de Technical Writing! 👩‍🏫

Déjame contarte un poco de mí. Mi nombre es Nancy Salazar, de profesión soy Ingeniera en Tecnologías de la Información y me especialicé en el área de Sistemas Informáticos. Soy de México y me encanta escribir desde que tengo uso de razón.

Cuando comencé mi carrera en la universidad tuve mi primer acercamiento con la magia del hardware y software. Y fue justo durante este proceso que conocí que no solo se trataba de programar y programar, sino que también tienes que documentar, algo que a la mayoría de mis compañeros les daba flojera. De hecho, algo me dice que es una actividad que sigue sin gustarles.

![](https://i.ibb.co/8KCYVsJ/prof.webp)

Como podrás imaginar, en los trabajos en equipo en los que teníamos que desarrollar algún proyecto de software yo era la que hacía toda la documentación. Para mí era fascinante, ya que, como comenté en líneas anteriores, me encanta la escritura. No sabía que en algún punto de mi carrera iba a combinar mi amor por la escritura con el software.

A lo largo de mi trayectoria profesional me he ido involucrando en más proyectos en los que puedo colaborar como Technical Writer. Incluso escribo artículos para revistas y plataformas en línea donde trato temas relacionados con tecnología y emprendimiento, tecnología y cine… tecnología y cualquier tema. Espero que todo lo que te comparto en este curso de introducción te motive a convertirte en _Technical Writer_.

  #### Definición de Technical Writing 🤓

La traducción literal de technical writing es “Escritura Técnica”. Pero vamos un poco más a las raíces, veamos qué dice la Real Academia Española (RAE) en torno al significado de la palabra Técnica con algunas de sus definiciones:

**Técnico, ca**

> Del lat. mod. technicus, y este del gr. τεχνικός technikós, der. de τέχνη téchnē ‘arte’.

1. adj. Perteneciente o relativo a las aplicaciones de las ciencias y las artes.

2. adj. Dicho de una palabra o de una expresión: Empleada exclusivamente, y con sentido distinto del vulgar, en el lenguaje propio de un arte, ciencia, oficio, etc.

3. f. Conjunto de procedimientos y recursos de los que se sirve una ciencia o un arte.

4. f. Pericia o habilidad para usar una técnica.

  #### ¿Cómo me convierto en Technical Writer? ✊

Google dice que no hay un camino único para convertirte en _technical writer_. Hay una amplia gama de antecedentes educativos y profesionales que hay detrás de esta profesión. Te encontrarás con muchos ingenieros de software, ingenieras de operaciones de desarrollo, periodistas, físicos, abogadas y profesores. Somos un grupo diverso, pero todos compartimos las siguientes habilidades:

  + Escribir claramente en tu idioma natal.
  + Escribir claramente en inglés también.
  + Aprender tecnologías complejas con relativa rapidez.
  + Explicar tecnologías complejas de manera útil para el público objetivo.
  + Tener fuertes habilidades interpersonales.
  + Comprender el código de lenguajes de programación.

  ### Nuestra misión como technical writers 🏁

En resumen, los technical writers somos híbridos raros, poseemos una mezcla poco común de talentos. Tenemos por objetivo transformar la información técnica en un lenguaje fácilmente comprensible y cumplimos este reto a través de la escritura técnica, obteniendo como resultado información útil, puntual y fácil de interpretar.

  **Pero ¿para qué todo esto?**

  Ayudamos a las personas a utilizar aplicaciones informáticas, evaluar una condición médica, prevenir accidentes, operar equipos industriales, entre muchas otras funciones. Transmitimos información técnica de manera escrita y fácilmente comprensible para todas las personas.

_¿Sabías en qué consiste technical writing?_ ¿Has colaborado escribiendo documentos técnicos en algún proyecto interesante? ¿O estás emocionada por descubrir este nuevo mundo desde cero? Estoy encantada de conocer tu experiencia en este trabajo y ayudarte a descubrir cómo puedes mejorar. 🤝

¡Te espero en la próxima clase! Vamos a estudiar la importancia de escribir específicamente para nuestra audiencia. 💪

  ## Habilidades para convertirte en Technical Writer

![](https://i.ibb.co/F8JMsQW/write.webp)

  ## ¿Conoces a tu público? Escribe específicamente para tu audiencia

  ### Importancia de identificar a tu audiencia y fases de la escritura 🎯

  Uno de los pilares del technical writing es la **identificación de la audiencia** a la que va dirigido tu escrito, independientemente de que sea un informe técnico o no.

¿Quiénes van a leer tus escritos? ¿Cuáles son sus perfiles? ¿Qué es lo que saben y qué es lo que no saben? Todas estas son las preguntas más importantes que debes hacer. Con base en las respuestas que obtengas podrás cubrir todas las fases de la escritura de un documento:

  - Planificación
  - Redacción
  - Revisión

  En próximas clases profundizaremos un poco más en todas estas fases.

> ¿Para quién? La importancia de una audiencia definida

Dicho en otras palabras, definir a tu audiencia no es más que la “adaptación” de tu escritura para cubrir las necesidades, intereses y antecedentes de la gente que leerá tus escritos.

![](https://i.ibb.co/XD1V8x9/people.webp)

  ### Analiza a tu audiencia 🕵️‍♀️

En un proceso de documentación global se invierte mucho tiempo y esfuerzo en definir la audiencia, lo hacemos a través de encuestas, análisis de la experiencia de los usuarios, documentación de pruebas, entre otras. Para fines prácticos de este curso, vamos a hacerlo de una manera sencilla, con algunas preguntas básicas que enlisto a continuación:

1. ¿Quién es tu audiencia? ¿Quiénes serán tus lectores?

2. ¿En cuál compañía trabajan? ¿Cuál es su rol?

3. ¿Cuál es su profesión? ¿En qué área son expertas?

4. ¿Qué necesitan? ¿Cuál es su interés por cubrir?

5. ¿Cómo usará tu audiencia la información de tus escritos?

6. ¿Cuál es el perfil técnico de tu audiencia? ¿Qué saben y qué no saben sobre tu tema?

7. ¿Hay alguna limitación técnica entre tu audiencia y tu proyecto de escritura?

Quizá estas preguntas te parezcan muy absurdas, pero te doy un consejo: **jamás des por sentado algo**. Hay tantas cosas que dejamos pasar de largo porque creemos que son bastante obvias, cuando la realidad es que son de los puntos en los que más debemos de prestar atención. **La falta de análisis** y la **adaptación de la escritura** son de los problemas más comunes en documentos profesionales. Dirigirse a un público equivocado puede ser bastante complicado.

Todo lo anterior podría resumirse en una simple ecuación:

> **Buena documentación** _= conocimientos y habilidades que tu audiencia necesita - conocimientos y habilidades que tu audiencia tiene_

  ### Tipos de Audiencia 👨‍👩‍👧‍👦

Identifica los tipos de audiencia, los roles son una aproximación para definirlos. La división más común es la siguiente:

  - **Expertos:** personas que conocen la teoría y el producto por dentro y por fuera.

  - **Técnicos:** personas que construyen, operan, mantienen o reparan lo que los expertos diseñan.

  - **Ejecutivos:** personas que toman decisiones comerciales, económicas, administrativas, legales, gubernamentales y/o políticas sobre las cosas con las que trabajan los expertos y técnicos.

  - **No técnicos:** personas que tienen curiosidad por un asunto técnico específico y quieren aprender sobre él, pero sin ninguna razón específica o práctica.

Las personas dentro de un mismo rol normalmente tienen compatibilidad en ciertas habilidades y conocimientos técnicos. Por ejemplo, la mayoría de ingenieros de software conocen algoritmos, estructuras de programación, rendimiento de algoritmos, notación Big-O y al menos un lenguaje de programación. Sin embargo, el hecho de que conozcan acerca de notación Big-O no es sinónimo de que los roles no técnicos conozcan de qué trata esta.

Otro ejemplo es cuando leemos una publicación en el periódico relacionada con alguna investigación. Este artículo evidentemente usará un lenguaje dirigido a un público no especializado. Por otro lado, el informe de investigación para científicas o profesionales del tema tendrá un aspecto diferente.

Escribir sería más sencillo si todo el mundo compartiera el mismo rol y conocimiento, pero lamento informarte que no es así. Ismael es experto en Ruby. Ana es experta en Android. A Héctor le encanta Linux. Facundo sabe mucho de Python. Con esto quiero decir que los roles son insuficientes, por lo cual, también debes de considerar la proximidad de tu audiencia al conocimiento de cada uno.

  ### Ejemplo: analicemos la audiencia de QIUB 😎

Vamos a hacer un ejemplo de análisis de audiencia para el proyecto ficticio QIUB.

Mi audiencia se divide en:

  - Ingenieros de Software

  - Ejecutivos

La proximidad de conocimiento en mi audiencia es la siguiente:

  - Mi audiencia conoce las API de QIUB que son parecidas a las API de LEXO (otro proyecto ficticio).

  - Mi audiencia conoce teóricamente el lenguaje de programación Python, pero toda su vida solo han programado proyectos en JavaScript.

  - Mi audiencia sabe de matemáticas discretas, pero necesitan repasar el tema de árboles.

Para determinar lo que tu audiencia necesita para cumplir la ecuación que te presenté líneas atrás la recomendación es que escribas en una lista todo lo que tu audiencia necesita aprender. Por ejemplo:

  - Usar la API de QIUB para agrupar productos por categoría.

  - Usar la API de QIUB para agrupar clientes por sector.

  - Usar la API de QIUB para agrupar los productos más consumidos.

  Siguiendo estos pasos introductorios estoy segura de que tu audiencia será más clara y así evitarás escribir para un público erróneo.

  ### Ejercicio: mejora un texto de introducción a Python 🐍

Imaginemos que el siguiente texto va dirigido a personas que nunca en su vida han programado y que tendrán su primer acercamiento a la programación a través de Python. ¿Cómo lo mejorarías?

> “Python es un lenguaje de programación divertido, extremadamente fácil de usar y que en los últimos años se ha vuelto muy popular. Es un lenguaje de programación interpretado cuya filosofía hace hincapié en una sintaxis simple y es multiparadigma, esto nos dice que Python es un lenguaje que soporta más de un paradigma, suponiendo paradigma como modelo de desarrollo, debido a que soporta orientación a objetos, programación imperativa y en menor medida programación funcional. Es interpretado de tipado dinámico y multiplataforma”.


  ## Cómo entrevistar equipos de programación para recolectar información técnica

Las opciones para recolectar información son un poco limitadas cuando se trata de documentar bloques de código o proyectos de software. Uno de los caminos más seguros es acercarte a platicar con el equipo de programación porque, además de ser especialistas en el tema, son quienes colaborarán contigo en la recolección de información.

Recordemos que tú eres la persona responsable de interpretar y transferir la información recabada a otras personas. Es por ello que primero debes de conocer lo que sabe el equipo de programación. Este punto también es otro de los pilares del technical writer, así que tienes que pulir tus habilidades para interactuar con personas de este nivel.

![](https://i.ibb.co/PcNmdfG/pc.jpg)

A continuación, te compartiré herramientas y recomendaciones a considerar antes y durante la recopilación de información para que esta sea todo un éxito.

### Herramientas previas para recopilar información 🛠

Una vez que ubiques a las programadoras que te ayudarán a recopilar la información para documentar la pieza de software en cuestión, debes comenzar con la preparación de una **entrevista**, que es la herramienta recomendada para este tipo de actividades y la cual te sugiero que tenga esta estructura:

  - Datos generales del entrevistado, el entrevistador y la fecha.
  - Breve introducción del tema u objetivo de la entrevista.
  - Cuestionario con preguntas abiertas e inclinadas a los temas definidos que te interesan indagar.
  - Espacio para comentarios y/u observaciones.

Además, utilizar otras herramientas como la documentación disponible del software a documentar es un buen arma, principalmente para no hacer preguntas incongruentes al programador. Pregunta si existe alguna documentación, léela con lujo de detalle y así podrás organizar preguntas útiles y enfocadas al contexto de lo que quieres saber del software.

También recuerda que tienes internet. Utiliza esta biblioteca digital para buscar información que creas conveniente y que los programadores suponen que ya conoces. No defraudes las expectativas que tienen de ti.

Una técnica que funciona bastante bien es probar el propio software por tu cuenta. Si está disponible, ¡adelante, pruébalo! Probar su funcionalidad garantiza una mayor orientación hacia dónde enfocarás tu entrevista y, de esta manera, la formulación de nuestras preguntas será más precisa.

### Recomendaciones previas a la entrevista 🎙

Es importante tomar ciertas recomendaciones previas a la entrevista:

  - **No presiones** a las programadoras, es evidente que tienen toda la información técnica que tú necesitas, así que es poco probable que una actitud intimidadora sea efectiva.

  - **No ruegues** a un programador para que te dé información, no llegarás muy lejos.

  - **A nadie le gusta que los saturen con montañas de correos electrónicos y recordatorios**. Quizá en un inicio funcione, pero sobre la marcha lo aborrecerás a tal grado que lo único que obtendrás de la programadora será poca o nula participación.

  - **Sé detallista** con los programadores. En las reuniones que tengas procura llevar algún detalle que la haga sentir a gusto durante la reunión, de esta manera lograrás que la entrevista sea más ligera.

  - **Haz amistad** con los programadores. Esta técnica funciona bastante bien, sé empático y descubrirás que será mucho más fácil obtener información.

  - **Investiga por tu cuenta** acerca de la pieza de software que documentarás. Sería un gran error de tu parte llegar e improvisar. Para la programadora será una falta de respeto hacia su tiempo y experiencia. Además, perderás credibilidad.

  ### Tips para recolectar información ✌️

  - La información recoléctala en entrevistas 1:1 (uno a uno) con el programador, convoca a una reunión proponiendo fecha, horario y lugar adecuado.

  - Llega puntual, agradece a la programadora por su tiempo y compártele por qué necesitas la entrevista.

  - Ten a la mano las herramientas que utilizarás, ya sea que escribas en tu laptop o con libreta y bolígrafos. Procura llevar tu smartphone con batería para que puedas grabar la entrevista en formato audio y para que tomes fotografías en caso de que hayan hecho apuntes en algún pizarrón.

  - No escribas literalmente toda la información que te brindan, toma nota de conceptos técnicos, datos, números e información que consideres importante.

  - Si te quedaste con dudas en alguna explicación, pídele a la programadora que te replantee la respuesta.

  - Ten el control de la entrevista. No permitas que se desvirtúe ni que se salga del objetivo acordado.

  - Una vez que tengas tu reporte de la entrevista, compártelo con la programadora y propón una fecha límite para recibir su retroalimentación y validación.

Para cerrar este capítulo te comparto esta frase de James Baker que va muy _ad hoc_ con este tema:

  > “La preparación adecuada previene el mal desempeño”.

  ### ¿Tienes todo listo para tu entrevista? 🤓

Para finalizar escribe en los comentarios cuáles elementos debe tener tu formato de entrevista antes de realizarla. Además, si tienes experiencia realizando este tipo de entrevistas, puedes mencionarnos qué otro tip o recomendación te ha ayudado en tu trabajo.

# 2. Estructura gramatical

  ## Un repaso por la gramática básica

La gramática es el arte de hablar y escribir **CORRECTAMENTE** una lengua. Y es esencial en el _technical writing_, ya que como creador de documentos técnicos, tu escritura debe de ser clara y gramaticalmente correcta.

![](https://i.ibb.co/pPPJKCL/grammar.jpg)

Sería incongruente dedicarte a escribir y que no sepas aplicar los conceptos de la gramática básica, así que demos a nuestra mente un refresh por aquellos términos que a veces ya damos por sentados y que no está de más dar un breve repaso.

![](https://i.ibb.co/ZTsDb4t/gra.jpg)

### Sustantivos 🕺

Los sustantivos representan objetos, personas, animales o lugares con nombre propio. Alejandra, México y taza son sustantivos, pero también lo son conceptos intangibles como la palidez o la optimización. Identifiquemos los sustantivos en la siguiente oración:

  - “Los **drones** son parte de la **transformación** digital que vivimos hoy en día”.

  - “**La impresión 3D** está empezando a ocupar cada vez más **espacio** en el **cine**”.

Si pensamos en código, las clases, variables y objetos funcionan como los sustantivos de tu programa.

### Pronombres 💃

La función principal del pronombre es la de sustituir al sustantivo o hacer referencia a este. De esta forma, ayuda a no repetir varias veces un sustantivo dentro del enunciado utilizando otros términos tales como “él”, “ese” o “aquel”.

  - “José se veía preocupado. Su madre **le** había llamado muchas veces, pero **él** no contestó el teléfono”.

En el enunciado anterior, la primera frase nos señala que “José” es el sustantivo y en la segunda frase los pronombres “le” y “él” sustituyen al sustantivo.

### Verbos 🏃‍♀️

Un verbo es una parte importante de las frases que describen un suceso, una acción mental/física o la existencia de una condición o un estado (existir, ser). Cada frase que escribas debe contener al menos un verbo y este representa la relación entre el actor y el objetivo.

  - “A Patrick le **gusta** el vino”.
  - “Carlos **estudia** en línea”.
  - “Los niños **aprenden** robótica”.
  - “Mi hermano **publicó** su investigación”.
  - “Alberto **trabaja** en el día y **estudia** en la noche”.
  - “Karla **iba cantando** en la calle”.

  ### Adjetivos y adverbios ✌️

Los adjetivos califican al sustantivo, veamos un ejemplo de ello:

  - “Gerardo compra cámaras **nuevas** para grabar videos **profesionales**”.

Los adverbios modifican el significado del verbo, de otro adjetivo o de otro adverbio. Por ejemplo, nota como el adverbio (eficazmente) modifica el verbo (ejecuta):

  - “La computadora **ejecuta** varios programas **eficazmente**”.

El adverbio no debe estar separado del verbo, también puede estar junto a este:

  - “La computadora **ejecuta eficazmente** varios programas”.

  ### Preposición 🤝

Las preposiciones son palabras invariables que se utilizan para establecer una relación de dependencia entre dos o más palabras.

  - “Los programas escritos **con** JavaScript se pueden probar directamente **en** cualquier navegador **sin** necesidad **de** procesos intermedios”.

  ### Conjunción y transición 🖇

Las conjunciones son palabras que se utilizan para unir dos o más partes de una oración o dos o más oraciones. Las más comunes son: “y”, “pero” y “o”.

  - “Nos creemos muy listos **y** muy mayores, **pero** nos hemos perdido una infancia entera de introducción a la programación **o** nunca tuvimos interés en ello”.

Las transiciones más importantes usadas en technical writing son: “por ejemplo”, “sin embargo”, “en primer lugar” y “además”. A continuación te comparto un ejemplo de esto:

```bash

"El Manual de Instalación tiene como objetivo servir de guía en la instalación del sistema. En primer lugar, deberá especificar los requerimientos hardware y software necesarios para el correcto funcionamiento del sistema, para posteriormente describir cada uno de los pasos necesarios (por ejemplo: la configuración, compilación e instalación del sistema). Además, se deberán incluir las pruebas que se deberán realizar para asegurar que la instalación se ha realizado correctamente, así como el procedimiento de marcha atrás a aplicar en caso de que no haya resultado exitosa la instalación del sistema".

```

La gramática también forma parte importante de la escritura y es de gran relevancia para transmitir ideas e información. Así que, si aspiras a hacer un buen trabajo como technical writer, debes de estar bien preparado y dominar a la perfección tu gramática.

  ### ¿Sabes diferenciar estos conceptos? 🕹

Como reto de esta clase debes identificar en las siguientes oraciones cuáles son sustantivos, pronombres, verbos, adjetivos, adverbios, conjunciones y transiciones.

**Identifica el sustantivo:**

  - avaScript es uno de los más potentes e importantes lenguajes de programación en la actualidad.

**Identifica el pronombre:**

  - El primer estándar que creó el comité TC39 se denominó ECMA-262, en él se definió por primera vez el lenguaje ECMAScript.

**Identifica el verbo:**

  - Los programadores prefieren la denominación ECMAScript.

**Identifica el adjetivo y el adverbio:**

  - El requisito imprescindible para cualquier programador es un formulario principalmente.

**Identifica la conjunción y transición:**

  - El DOM permite a los programadores web acceder y manipular las páginas XHTML como si fueran documentos XML. Además, manipula de forma sencilla los documentos XML.

  ## Voz activa vs. voz pasiva: estándares y estructura de una oración

La **voz verbal** es todo un fenómeno. Sirve para marcar la relación que tiene el sujeto con el verbo. El verbo tiene dos voces: **voz activa y voz pasiva**. Aunque en español es un poco difícil de comprender, la usamos de manera inconsciente en nuestro día a día, así que quizás ahora que leas este capítulo prestes atención a la estructura que das a tus oraciones, ya sea al hablar o escribir.

  ### Definición de voz activa y voz pasiva 🤓

La **voz activa** es el estándar para el technical writing. La mayor parte de las frases en la escritura técnica están en voz activa debido a que es más corta e interesante de leer. Además, la voz activa muestra claramente al sujeto en una situación, cuando leemos en esta voz gramatical sabemos quién hace qué y a quién.

Por otro lado, está la **voz pasiva**. Como su nombre lo indica, el sujeto aparece como paciente. Se hace énfasis en un estado o en una acción.

  ### Ejemplos de voz activa y voz pasiva 💥

  - **Voz activa:** Ellos hablan el idioma francés.
  - **Voz pasiva:** El idioma francés es hablado por ellos.

La voz activa nos muestra que los actores son “Ellos”, en cambio, con la voz pasiva “ellos” quedan opacados.

  - **Voz activa:** El estudiante edita el archivo.
  - **Voz pasiva:** El archivo es editado por el estudiante.

De nuevo, la voz activa nos muestra que el actor es “El estudiante” y en la voz pasiva “el estudiante” queda sin mucha importancia.

Las frases que tienen la palabra “por” son casi siempre pasivas y sus verbos terminan en tiempo pasado participio: “hablado”, “editado”, etcétera. No obstante, si tienes una frase pasiva, la puedes convertir en activa. Solo pones al actor de tu oración al inicio y listo.

  ### Estructura para oraciones cortas 👷‍♀️

En resumen, podríamos estructurar las oraciones cortas de la siguiente manera:

  - **Voz activa** = sujeto + verbo + objetivo.
  - **Voz pasiva** = objetivo + verbo + sujeto.

  ### Identifica verbos pasivos 🕵️‍♀️

Puedes identificar fácilmente los verbos pasivos, normalmente siguen la siguiente fórmula:

  - **Verbo pasivo** = verbo ser + verbo en participio.

Ejemplos:

  - Fue seleccionado.
  - Es reconocido.
  - Es programado.
  - Es llamado.
  - Fue dichoso.

Los **verbos en participio** normalmente terminan en -ado y en -ido. Algunos ejemplos son:

  - Leído
  - Encontrado
  - Huido
  - Olvidado
  - Recomendado

Es importante mencionar que existen **algunos verbos en participio que son irregulares**, es decir, que no necesariamente terminan en -ado o en -ido. Por ejemplo:

  - Propuesto
  - Roto
  - Expuesto
  - Hecho

Si la frase contiene un sujeto, normalmente la preposición “por” sigue al verbo pasivo. Esta preposición suele ser una ayuda para detectar la voz pasiva. Ejemplos:

  - Fue seleccionado **por** el jurado.
  - Es reconocido **por** la universidad.
  - Es programado **por** los estudiantes.

  ### Los verbos imperativos son voz activa 😌

Un **verbo en modo imperativo** se usa para dar una **indicación, orden o consejo**. Cuando hacemos listas (tema que veremos en un futuro) lo recomendado es que las frases inicien con un verbo en modo imperativo. Por ejemplo:

  - **Pulsa** sobre «Nuevo» para crear una partición nueva.
  - **Elige** el tamaño de la nueva partición.
  - **Reinicia** el sistema varias veces.

Estas frases que usan al inicio un verbo en modo imperativo están en voz activa, aunque no mencionan directamente a un sujeto, ya que **el sujeto implícito eres túo.**

  ### ¡Practiquemos ejemplos complejos de voz activa y pasiva! 💪

Este tema es complejo y traté de hacerlo lo más digerible para ti. Las frases que te compartí en los ejemplos anteriores son sencillos y con un solo verbo. Adicional a lo explicado, tengo que compartirte que a lo largo de tu profesión como technical writer te encontrarás con frases que contienen más de un verbo. Te comparto unos últimos ejemplos.

En esta oración todo es voz pasiva:

  - El **bloque** de su código **es ejecutado** en el navegador y como un comentario **es insertado** el bloque de su código.

Esta es la misma oración convertida parcialmente a voz activa:

  - El navegador ejecuta el bloque de su código y como un comentario el bloque de su código **es insertado**.

Misma oración convertida en su totalidad a voz activa:

  - El navegador **ejecuta** el bloque de su código y **se inserta** como un comentario el bloque de su código.

  ### La voz activa es mejor que la voz pasiva 🔥

Te recomiendo que uses con mayor frecuencia la voz activa que la voz pasiva. Cuando leemos nuestra mente tiende a convertir la voz pasiva en activa. Si usamos la voz activa, ahorramos tiempo a nuestro lector en procesar la información.

La voz pasiva da muchas vueltas a algo sencillo. Sí, informa la acción, pero de manera indirecta y opacan al sujeto por completo. La voz activa es mejor que la voz pasiva porque te ayuda a ser corto, claro y directo.

  ### ¿Puedes cambiar el tipo de voz en una oración? 👊

Como reto de esta clase reescribe estas oraciones de voz pasiva a voz activa:

  - Varios recursos son descritos en los archivos del directorio values/.
  - Un único recurso es definido en cada campo secundario del elemento .
  - Son tus recursos predeterminados los recursos que han sido guardados en los subdirectorios definidos.


  ## Uso correcto de acrónimos y abreviaturas para explicar términos desconocidos

Un tema particular del technical writing cuando escribimos documentos es utilizar de manera frecuente terminología desconocida para algunos o la mayoría de los lectores. Esta terminología es importante ubicarla y definirla. Las siguientes tácticas te ayudarán con esto.

  ### Explica términos nuevos o desconocidos 😅

Conforme vas forjando tus habilidades de escritura aprenderás que algunos términos pueden ser **desconocidos para algunos o todos tus lectores.** Incluso encontrarás términos desconocidos para ti. Es por ello que nuestro deber es comprender dichos términos y ligarlos a explicaciones, comparaciones o ejemplos para que nuestros lectores tengan una referencia más tangible.

Si tu documento gira en torno a ese término desconocido, define desde un principio su significado. Pero si tu documento tiene muchos términos desconocidos, lo más recomendable es que hagas un glosario con todas las definiciones.

  ### Usa términos de forma coherente 🙏

Supongamos que escribiste algunas líneas de código y a una variable llamada nombre le cambias el nombre por `name` a la mitad de la ejecución de un método. Es evidente que tu código no compilará. Eso mismo pasa cuando escribes un documento y usas un término de manera frecuente, si a mitad del documento lo cambias, el hilo de tu texto se perderá y las ideas en las cabezas de tus lectores no “compilarán”.

Para evitarlo **utiliza el mismo término que has usado a lo largo de tu documento**. Una vez que ya estás usando un término a lo largo de tu escritura, no lo modifiques ni lo renombres.

  ### Usa abreviaturas 💕

Existen casos en los que algunos conceptos se pueden usar de manera abreviada y luego puedes usar esa abreviatura durante todo el documento.

> “La **tecnología educativa**, es decir, la **EdTech**, se ha convertido en la herramienta que enriquece el proceso de enseñanza de niños y adultos. La **EdTech** orientada a niños y adolescentes busca que estos aprendan de una forma más sencilla a través de la personalización de su educación”.

> “**Wireless Fidelity (WiFi):** este término fue creado por una empresa de branding y solo se hizo popular en su forma abreviada. Al igual que los transistores de radio tradicionales, las redes **WiFi** transmiten información por el aire utilizando ondas de radio”.

  ### Usa acrónimos adecuadamente 🤏

El uso de acrónimos es algo común en cualquier escrito técnico. El acrónimo tiene cierta similitud con las siglas, la única diferencia es que un acrónimo puede formarse tomando tanto la inicial como la primera parte de las palabras que forman el nombre completo, en cambio, la sigla siempre se forma con la letra inicial.

Si usas un acrónimo desconocido en tu documento, lo ideal es que escribas el término completo y después el acrónimo entre paréntesis, ejemplo:

> **Hypertext Transfer Protocol Secure (HTTPS)** es un protocolo de comunicación de internet que protege la integridad y la confidencialidad de los datos de los usuarios entre sus ordenadores y el sitio web. Te recomendamos que adoptes **HTTPS** para proteger sus conexiones con tu sitio web, independientemente de lo que este contenga". 

> Los datos que se envían mediante **HTTPS** están protegidos con el protocolo **Seguridad en la capa de transporte (TLS).**

Si defines el significado completo del acrónimo en su primera mención, es suficiente. Ya sobre el texto puedes usarlo y el lector se irá familiarizando con el acrónimo. Aunque pasa algo curioso con los acrónimos, ya que sobre la marcha van generando una identidad, tanto así que llega el momento en que nuestros lectores solo recuerdan el acrónimo en sí, pero no su significado tal cual. Ejemplos actuales de esto son: “www”, “HTML”, “SaaS”, “HTTPS”, entre otros.

Las sugerencias puntuales para el uso de acrónimos son:

  - Si el acrónimo será usado pocas veces, no es necesario definirlo.
  - Si el acrónimo es más corto que el término completo y lo vas a usar muchas veces en el documento, sí es necesario definirlo.

Como verás, algunos términos quizá sean muy familiares para nosotros, pero habrá personas que ni sabían de la existencia de estas palabras. Así que enfócate siempre en quienes te leerán y trata de ser lo más claro posible en cada uno de tus documentos técnicos sin caer en palabras rimbombantes o en palabras que parecen sacadas de otro planeta.

# 3. Técnicas de escritura fundamentales para documentos técnicos

  ## Sigue las reglas de George Orwell para escribir con claridad

En el _technical writing_ la claridad es primordial por encima de todo. Es un tema indiscutible. Y para ello las frases son la unidad mínima en las que podemos expresar una idea. Tus frases deben ser claras y efectivas, así tus textos se entenderán mejor, el lector captará el mensaje fácilmente y tú harás gimnasia mental para ordenar tus pensamientos. **Recuerda que a veces escribimos mal porque pensamos mal**.

Hagamos una comparación con programar. Las buenas prácticas nos dicen que escribir líneas cortas de código facilita su lectura para otros. Además, es más práctico darle mantenimiento a líneas cortas de código que a líneas extensas. Esto aplica de la misma manera en el _technical writing_. **La documentación corta es más fácil de leer y darle mantenimiento que a una documentación larga.**

Te compartiré las bases para que aprendas a escribir tu documentación de manera corta, correcta, efectiva y, principalmente, con claridad.

  ### Sé breve 🏃‍♀️

Iniciemos con esta frase del filósofo francés Blaise Pascal:

> “Te escribo una carta larga porque no tengo tiempo de escribir una corta”.

Y tiene razón. **Es más difícil escribir frases cortas que frases largas**. Redactar frases cortas nos obliga a organizar nuestro pensamiento y ordenar la información. El estilo breve es más exigente y requiere mucha disciplina, pero es la manera más eficaz para ser claros en nuestros textos.

Todo esto tiene que ver con nuestro pensamiento. Cuando tenemos que escribir sobre cualquier tema las ideas comienzan a dispararse, comenzamos a escribirlas en bruto y al finalizar tendremos frases kilométricas. Por sentido común, no podemos dejarlas así, tenemos que acortarlas para que nuestro lector no abandone nuestros textos.

Si tienes frases muy largas, es señal de que tienes material para sacar de ahí dos o tres frases breves. Tampoco tienes que escribirlas a manera de telegrama, hay que saber balancear, cada frase puede ser un concepto o una idea. Por otra parte, también debo mencionarte que es bueno agregar de vez en cuando alguna frase larga para no hacer monótona nuestra lectura, solo recuerda ser moderado al momento de usarlas.

> [Cómo escribir bien para internet](https://www.youtube.com/watch?v=2cYAN4deohw)

Aquí te muestro un ejemplo de una frase muy larga con múltiples conceptos:

> “El inventor de Linux es el ingeniero de software finlandés Linus Benedict Torvalds, quien llegó al mundo de la informática desde una edad muy temprana y estudió Ciencias de la Computación en la Universidad de Helsinki. Durante aquella época, él decidió adquirir un nuevo PC 386 -33 Mhz, 4MB de RAM (uno de los más avanzados de su época), sin embargo, a Linus no le gustaba el sistema operativo con el que trabajaba –Minix– y decidió crear uno él mismo”.

Si dividimos esa frase larga en varias frases cortas sin romper la esencia de la idea, el resultado sería el siguiente:

> “El inventor de Linux es el ingeniero de software finlandés Linus Benedict Torvalds. Ligado al mundo de la informática desde una edad muy temprana, estudió Ciencias de la Computación en la Universidad de Helsinki. Durante aquella época decidió adquirir un nuevo PC 386 -33 Mhz, 4MB de RAM (uno de los más avanzados de su época). Sin embargo, no le gustaba el sistema operativo con el que trabajaba –Minix– y decidió crear uno él mismo”.

  ### Usa verbos fuertes 😈

En el *technical writing* los verbos son lo más importante de una frase. Selecciona los verbos adecuados y el resto de la frase se da por sí misma. Quizá te tome tiempo elegir el verbo adecuado, pero una vez que lo hagas tendrás resultados satisfactorios. **Trata de usar verbos precisos, fuertes y específicos.**

Por ejemplo:

  **Verbo débil:** Este bug **pasa** cuando…
  **Verbo fuerte:** El sistema **muestra** un bug cuando…

  ### Elimina los “hay” 🙅‍♀️

No aburras a tus lectores usando la palabra “hay”. Dales sujetos y verbos reales. Considera la siguiente frase de ejemplo:

  > Hay una variable llamada **mensaje** que almacena el mensaje que se mostrará al usuario.

Con un poco de trabajo podemos mejorarla, por ejemplo, de esta forma:

  > **Una variable** llamada mensaje almacena el mensaje que se mostrará al usuario.

  ### Elimina o sustituye las palabras superfluas 🚫

Las palabras superfluas o de relleno son las que no tienen ninguna función dentro de nuestras frases. Son “comida chatarra” para nuestro lector. **Es importante eliminar esas palabras o, en su defecto, sustituirlas**. Con la práctica las irás descubriendo.

  En este ejemplo cambiaré solo un par de palabras sin alterar su significado:

  > **A pesar de que** Gerardo no sabe programar, él sabe documentar software.

  > **Aunque** Gerardo no sabe programar, él sabe documentar software.

  ### Escribe con las reglas de George Orwell 📖

Para finalizar quiero compartirte las reglas generales de escritura de George Orwell, las cuales también funcionan para el technical writing:

  - Nunca uses una palabra larga cuando una corta funciona.
  - Si es posible recortar una palabra, hazlo.
  - Nunca uses la voz pasiva, usa la voz activa en su lugar.
  - Nunca uses una frase extranjera, una palabra científica o de la jerga si puedes pensar en un equivalente del inglés -o el idioma en el que redactes tu documento- cotidiano.

  ## Uso correcto de listas y tablas para ordenar información

Las buenas listas y tablas pueden transformar el caos técnico en algo ordenado. Generalmente **los lectores técnicos aman las listas**. Por lo tanto, cuando escribas busca oportunidades para convertir la prosa en listas. ¡Aquí te voy a decir cómo!

  ### Diferentes tipos de listas y sus usos 📜

Cuando escribimos documentos, independientemente de que sean técnicos o no, se debe tomar en cuenta que **nuestros lectores NO siempre leerán todo nuestro contenido**. Harán un escaneo rápido en nuestros documentos y buscarán los elementos de su interés.

**Las listas y las tablas son útiles porque hacen hincapié en elementos de interés**. Cuando vemos listas o tablas con elementos puntuales nuestra atención se enfoca en ello. Además de facilitar la lectura, también crean más espacio en blanco en nuestros documentos y extienden el texto para que las páginas no parezcan hojas estáticas repletas de letras.

  > ¿Tu texto tiene ritmo?

Cada tipo de lista tiene una función relevante dentro del _technical writing_. Estas nos ayudan a que los lectores logren comprender, memorizar y revisar los puntos clave del tema en cuestión, seguir una secuencia de acciones o instrucciones y, además, separa los largos tramos de texto y los separa en frases cortas, así como lo estudiamos en la clase de cómo escribir con claridad.

Sin embargo, a pesar de la diversidad de tipos de listas que existen, todas deben cumplir algunos lineamientos generales, los cuales mencionaré en el siguiente punto.

  ### Lineamientos generales de las listas 📚

En contextos de technical writing, debes utilizar un estilo específico de listas, como el que te presento aquí:

  - Utiliza las listas para resaltar o enfatizar el texto o para enumerar elementos secuenciales.
  - Usa espaciado, indentación y puntuación en cada frase.
  - Haz que los elementos de la lista sean paralelos en la redacción.
  - Asegúrate de que cada elemento enlistado tenga que ver con el enfoque de la lista.
  - Utiliza una introducción para presentar los elementos de la lista y para indicar el significado o propósito de esta (y puntuar con dos puntos).
  - Evita el uso excesivo de listas; el uso de demasiadas listas destruye su eficacia.
  - Puedes aprovechar la función “estilos” o “formatos” de tu software para crear listas verticales en lugar de construirlas manualmente.

  ### Elige el tipo de lista correcto 👍

Los siguientes tipos de listas dominan en la escritura técnica:

  - Listas con viñetas
  - Listas numeradas
  - Listas incrustadas o embebidas

Utiliza una lista con viñetas para los artículos no ordenados. Y utiliza una lista numerada para los artículos ordenados. En otras palabras:

  - Si reorganizas los artículos de una lista con viñetas, el significado de la lista no cambia.
  - Si reorganizas los artículos de una lista numerada, el significado de la lista cambia.

Por ejemplo, hemos hecho la siguiente lista con viñetas, la cual al reorganizar sus elementos no cambia el significado de la lista:

```JavaScript
Javascript es un lenguaje poderoso, sus usos más importantes son los siguientes:

- Desarrollo de sitios web del lado del cliente (frontend, en el navegador).
- Desarrollo de todo tipo de aplicaciones gracias a la plataforma NodeJS.
- Desarrollo de aplicaciones para dispositivos móviles, híbridas o que compilan a nativo.
```

La siguiente lista, por el contrario, debe ser una lista numerada, ya que si cambiáramos el orden de sus elementos, también cambiaría el significado de la lista:

```Javascript
Configurar una cuenta de GitHub:

1. Vaya a https://github.com/join.
2. Escriba un nombre de usuario, su dirección de correo electrónico y una contraseña.
3. Elija Sign up for GitHub y siga las instrucciones.
```

Una lista embebida contiene varios elementos dentro de una frase. Por ejemplo, la siguiente frase contiene una lista embebida con siete elementos:

```Javascript
CodeDeploy puede implementar una variedad de contenidos de aplicación prácticamente ilimitada, entre las que se incluyen código, funciones AWS Lambda sin servidor, archivos de configuración y web, ejecutables, paquetes, scripts y archivos multimedia.
```

En términos generales, las listas embebidas son una forma inadecuada de presentar información técnica. Lo adecuado es transformar las listas embebidas en listas con viñetas o listas numeradas. Veamos cómo quedaría la frase embebida presentada en el párrafo anterior a manera de lista con viñetas:

```Javascript
CodeDeploy puede implementar una variedad de contenidos de aplicación prácticamente ilimitada, entre las que se incluyen:

- Código
- Funciones AWS Lambda sin servidor
- Archivos de configuración y web
- Ejecutables
- Paquetes
- Scripts
- Archivos multimedia
```

  ### Inicia tus listas con un verbo imperativo 🤜

En la medida de lo posible, trata de iniciar tus listas con un verbo imperativo. **Un verbo imperativo es un verbo que le indica a alguien que debe hacer algo**. Por ejemplo: comprobar, revisar, editar o corregir son verbos que indican al lector lo que debe hacer. Observa cómo todos los elementos de la siguiente lista numerada inician con un verbo imperativo.

```JavaScript
Para poder usar AWS CodeDeploy por primera vez debes completar los pasos de configuración:

1. Aprovisiona un usuario de IAM.
2. Instala o actualiza y, a continuación, configura la AWS CLI.
3. Crea un rol de servicio para CodeDeploy.
4. Crea un perfil de instancia de IAM para las instancias Amazon EC2.
```

  ### Utiliza mayúsculas y puntuación adecuadamente 🔠

Si los elementos de las listas son frases, utiliza mayúsculas y puntuación, si no son frases, no las uses. Tomemos de ejemplo las listas presentadas en este capítulo:

Sin mayúsculas ni puntuación:

```Javascript
CodeDeploy puede implementar una variedad de contenidos de aplicación prácticamente ilimitada, entre las que se incluyen:

- código
- funciones AWS Lambda sin servidor
```

Con mayúsculas y puntuación:

```Javascript
Para poder usar AWS CodeDeploy por primera vez, debe completar los pasos de configuración:

1. Aprovisiona un usuario de IAM.
2. Instala o actualiza y, a continuación, configura la AWS CLI.
```

  ### Crea tablas útiles 🗂

Las tablas son esas filas y columnas de números y palabras. En su forma más simple, se conforma de filas y columnas. En la parte superior de la tabla lleva un encabezado y en el borde izquierdo pueden figurar encabezados de fila. Mayormente se usan para datos numéricos, pero tampoco te cierres a eso, encontrarás situaciones en las que se te presenten varios conceptos que tengan mismas categorías de detalle y te verás en la necesidad de usar una tabla.

A continuación encuentras un ejemplo sobre una tabla que muestra palabras claves del lenguaje Java que también son usadas en C#. Como verás, cumple con las características de la estructura descrita en los párrafos anteriores.

![](https://i.ibb.co/2v4GtwF/tabla.jpg)

  ### Estilo y formato 💅

Ten en cuenta algunas directrices específicas de estilo y de formato de las tablas:

  - En el texto que antecede a la tabla explica qué significa los datos dentro de ella. No dejes que tus lectores adivinen de qué se trata.
  - No abrumes a tus lectores con tablas inmensas de 15 columnas y 50 filas. Simplifica y no distorsiones los datos.
  - Si usas unidades de medida, no pongas la abreviatura o la palabra completa en cada celda. Pon la abreviatura entre paréntesis en el encabezado de la columna o de la fila.
  Si tus datos son números, alinéalos a la derecha.
  - Si tus datos son palabras, alinéalas a la izquierda o al centro.
  - Los encabezados de las columnas se centran en las columnas de datos numéricos y se alinean a la izquierda o al centro con las columnas de texto.

Las listas y las tablas son las mejores amigas tanto del technical writer como del lector. Ahora que sabes usarlas correctamente, utilízalas a tu favor.

  ### ¿Aprendiste a usar las listas a tu favor? 😎

Convierte el siguiente párrafo en una lista numerada:

  > Para cambiar la configuración regional en el emulador mediante el shell adb, elige la configuración regional que quieres probar y determina la etiqueta de idioma BCP-47; por ejemplo, el francés canadiense sería `fr-CA`. Después inicia un emulador y desde un shell de línea de comandos en la computadora host, ejecuta el siguiente comando: `adb shell` o si tienes un dispositivo adjunto, agrega la opción `-e: adb -e shell` para especificar que quieres el emulador. Finalmente, en el mensaje de shell `adb (#)`, ejecuta este comando: `setprop persist.sys.locale [*BCP-47 language tag*];stop;sleep 5;start.`

  **Establecer lista**

  Para cambiar la configuración regional en el emulador mediante el shell adb, elige la configuración regional que quieres probar y determina la etiqueta de idioma BCP-47, por ejemplo, el francés canadiense sería fr-CA.

```Javascript
  1. Inicia un emulador
  2. Desde un shell de línea de comandos en la computadora host ejecuta el siguiente comando:adb shell (o si tienes un dispositivo adjunto, agrega la opción -e: adb -e shell para especificar que quieres el emulador)
  3. En el mensaje de shell adb (#), ejecuta este comando:setprop persist.sys.locale [BCP-47 language tag];stop;sleep 5;start.
```

  ## Tipos de párrafos y paso a paso para estructurarlos

Los párrafos son la base de los documentos que redactamos como technical writers. En párrafos claros y precisos damos definiciones, explicamos un proceso o presentamos argumentos.

En la clase sobre cómo escribir con claridad nos quedó claro que una frase es la unidad mínima en la que se puede expresar una idea. En este capítulo descubriremos que los párrafos son la unidad mínima en la que se puede desarrollar una idea.

  ### ¿Qué es un párrafo? 👨‍💼

Un párrafo es la **mínima unidad de redacción que explica y desarrolla el significado de una idea**. Los párrafos generalmente tienen tres partes: **la oración principal**, las **oraciones argumentativas** y la **oración final**; todas ellas contienen la misma palabra o idea clave que controla la información en el resto del párrafo.

La oración principal tiene que ser una gran frase inicial. Hoy en día la mayoría de las personas están ocupadas y leen rápidamente centrándose en las frases iniciales, por lo tanto, **invierte tu energía en grandes frases iniciales**, estas son las que establecerán el punto central a tus párrafos.

  > Escribe la introducción de tu texto.

  ### Detalles a considerar en el propósito de tu párrafo 🤟

Para escribir párrafos efectivos debes tener en cuenta que los detalles específicos coincidan con el propósito de tu párrafo. En esta tabla te lo explico:

![](https://i.ibb.co/zQtZRWy/technic.webp)

  ### Paso a paso para escribir un párrafo 🔑

Para terminar te compartiré paso a paso cómo comenzarás a estructurar cada uno de tus párrafos:

![](https://i.ibb.co/TTCDyhJ/tec.webp)

Antes de cerrar este capítulo, te dejo esta frase para que continúes con mayor motivación:

  > El trabajo de escribir es simplemente esto: desenredar las dependencias entre las partes de un tema y presentar esas partes en un flujo lógico que permita al lector entenderlo.


# 4. Conceptos básicos de programación e ingeniería de software

  ## ¿Qué es programación? Evolución de la documentación y technical writing

Así como documentar no solo consiste en escribir texto, escribir tampoco es la única habilidad requerida para una _technical writer_, también necesitamos otras habilidades para usar herramientas profesionales y crear documentación. Los días en que las _technicals writers_ solo usaban Microsoft Word ya pasaron de moda, ahora se pone más atención en los detalles: imágenes, videos, ilustraciones, diagramas, etc.

Esto se debe a que la tecnología es un mundo igualmente inmenso a la cantidad de tipos de documentación que podemos encontrar; son varios los formatos en que puedes escribir: cartas de aplicación, instrucciones, descripciones, manuales de instalación y muchos otros.

  ### Familiarizándote con la programación 👨‍👨‍👧

Durante tu trayectoria se atravesará algún momento en el que tengas que documentar un bloque de código, APIs o proyectos de software. Estos elementos están creados con **lenguajes de programación**. Y para esto es obligatorio conocer de manera global qué es la programación, para que tus escritos sean enfocados a tus lectores con el fin de que comprendan cómo funciona cada bloque de código del proyecto de software que documentaste.

Como _technical writer_ __no es necesario que sepas programar como una desarrolladora__. El único objetivo es que estés familiarizada con los conceptos de programación, tener habilidades de leer código y otras competencias básicas. Cabe señalar que si sabes programar, son puntos a favor, si no lo sabes, no te preocupes, puedes aprender a hacerlo.

  > Si te preguntas por dónde empezar, te recomiendo el [Curso Gratis de Programación Básica](https://platzi.com/cursos/programacion-basica/) de Platzi. Será un gran complemento a tu carrera como technical writer.

  ![](https://i.ibb.co/xh5ZYdn/co.jpg)

Recuerda que a lo largo de tu profesión trabajarás con diferentes profesionales en diversos proyectos y deberás entender de qué te hablan. Como este curso está enfocado a crear documentos técnicos es primordial que conozcas principalmente terminologías y conceptos básicos de programación.

A continuación te explicaré qué es, cómo se hace y para qué sirve la programación sin hacer hincapié en cuestiones técnicas. **Solo quiero que entiendas qué es la programación en términos simples.**

  ### Aspectos básicos de la programación 👩‍💻

La **programación** es una forma de enseñar a los ordenadores a hablar nuestro idioma de la forma en que lo entendemos. La programación no se trata de aprender, sino de entender cómo transmitir nuestras necesidades a la computadora. Es un proceso de construcción de un programa ejecutable para una tarea computacional específica. Implica perfilar la precisión y generación de algoritmos además del consumo de recursos que utilizaremos.

Los algoritmos se crean usando lenguajes de programación. Estos lenguajes son un conjunto de símbolos y códigos que se usan para orientar la programación. A través de una serie de instrucciones, datos y algoritmos podemos controlar el comportamiento lógico y físico de una máquina. Para profundizar un poco más en estos conceptos te recomiendo que tomes la Carrera de Fundamentos de Programación de Platzi, aquí conocerás más a fondo las normas y buenas prácticas que hacen del código un texto legible.

  > Buenas Prácticas para Escritura de Código

Hasta ahora hay varios lenguajes de programación y seguirán creándose más. Los lenguajes de programación se clasifican en dos tipos:

  - **Los lenguajes de bajo nivel** son en su mayoría lenguajes ensambladores que hablan directamente con el procesador, por lo que necesitan menos compilación.
  - **Los lenguajes de alto nivel** son avanzados y poderosos para programar y obtener la salida que deseamos. En cada computadora los lenguajes de alto nivel se convierten en lenguajes de bajo nivel y luego alimentan al procesador para procesar las solicitudes.

  > No todos los lenguajes de programación son del más alto o el más bajo nivel. También hay muchos intermedios.

  Este trabajo lo hacen los **compiladores**, las herramientas encargadas de la conexión con las máquinas de forma programada. Convierten el lenguaje que escribimos en un lenguaje comprensible para la máquina.

  ### Lenguajes de programación modernos 👼

Según [Career Karma](https://careerkarma.com/blog/how-many-coding-languages-are-there/?fbclid=IwAR2B9sAuJvm4jJj_oe8jlxnzdjXeCdDWorB0lFNymNpuHsotFh6S25wnnec), en el mundo existen aproximadamente 9,000 lenguajes de programación, aunque los más populares e incluso los más modernos son alrededor de 50. Algunos ejemplos de lenguajes de programación modernos son C, C++, Swift, Java, JavaScript, Python, Ruby, C#.

La mayoría de estos lenguajes de programación son de alto nivel y tienen estos fundamentos básicos:

  - **Fiabilidad:** se trata de la frecuencia con la que el resultado de un programa es correcto.
  - **Robustez:** la facilidad con la que un programa puede detectar errores o fallos.
  - **Usabilidad:** cuánto mejor un programador puede usar el lenguaje para servir a su propósito.
  - **Portabilidad:** es el rango de compilación del lenguaje donde se ejecuta dependiendo del sistema operativo, el hardware.
  - **Mantenibilidad:** la facilidad de cómo el programa puede ser modificado para futuras actualizaciones o adiciones.
  - **Eficiencia:** medida de los recursos del sistema que un programa consume, como los ciclos de la CPU, la memoria, etcétera.

  ### Empezando con la programación 💪

Para tener tu primer acercamiento con el mundo de la programación no necesitas mucha experiencia. Si sabes **matemáticas** e **inglés básico**, puedes empezar fácilmente. Aprender a programar te ayudará cada día a abordar los problemas de manera diferente.

Mi sugerencia es empezar por lo básico. Lee blogs y libros sobre un lenguaje de programación de tu interés, toma cursos para principiantes (como los **cursos de introducción** que encuentras en Platzi) y encuentra algunos recursos en línea para practicar por tu cuenta. **Lo importante es que tengas idea de lo que son las funciones, las variables, las declaraciones, las condiciones y los bucles.** Siempre es mejor empezar por ahí cuando no tienes ningún conocimiento.

  > [La filosofía del programador: Ventajas de aprender a programar](https://platzi.com/blog/filosofia-programador/)

En verdad, si tienes oportunidad de aprender a programar, hazlo. Es una parte fundamental de nuestra preparación para saber lo que está sucediendo en nuestro mundo. La programación es como la meditación: te da diversión, conciencia de ti mismo y la capacidad de resolver problemas.

Para finalizar, quizás te preguntes si como technical writer necesitarás aprender un lenguaje de programación o no. Mi respuesta es que eso dependerá de los requisitos e intereses de tu trabajo. Pero, por supuesto, si aprendes un lenguaje de programación créeme que te ayudará a ascender más rápido en tu carrera.

  ### Escribe tu primer “¡Hola, mundo!” 👨‍💻👩‍💻

Ahora que ya tienes un poco más de contexto acerca de qué es la programación, ¿qué te parece si programas tu primer “¡Hola mundo!”? Es bastante sencillo, elige un lenguaje de programación que sea de tu interés, ¡el que quieras! Estoy segura de que de esta manera te quedará más claro el tema que vimos en esta clase. ¡Déjanos tus resultados en los comentarios!

  ## Lenguajes de programación, tipos de datos y estructura de documentos HTML

En este curso nos enfocaremos básicamente en crear documentos técnicos, aunque es probable que en tu trayectoria también te encuentres con que algún día debas documentar un bloque de código, una API o un proyecto de software.

Documentar código es todo un mundo, así que, siendo este un curso de nivel introductorio, te mencionaré de manera muy _light_ algunos conceptos básicos de programación, su definición corta y concisa.

  ### Tipos de datos 💁‍♀️

Un tipo de dato es la propiedad de un valor que determina su dominio. Es decir, qué valores puede tomar, qué operaciones se le pueden aplicar y cómo es representado internamente por la computadora.

Los tipos de datos más comunes son:

  - `char o string` (textos “¡Hola!”, “esto es un string”…)
  - `int` (12, 6, 873…)
  - `double o float` (1.5, 8.32, 3.1416…)
  - `booleano` (verdadero o falso)

  #### String 🔡

El tipo de dato string representa texto. Dependiendo del lenguaje de programación puedes encerrar ese texto en comillas sencillas `(')` o comillas dobles `(")`. Por ejemplo, `‘Hello, world!’` o `“Hello, world!”`.

La concatenación funciona para unir palabras y puedes usar el signo `“+”` para hacerlo.

Por ejemplo:

  - `"Hello" + "World"` sería `"HelloWorld"`

Si lo notas, no hay un espacio entre las palabras, por lo cual, debes agregarlo:

  - `"Hello" + " " + "World"` o también `"Hello " + "World"`.

Para dar un salto de línea debes usar caracteres especiales, en este caso:

  - `\n` es usado para crear una nueva línea.
  - `\t` es usado para tabular.

Por ejemplo:

`“Hello \nWorld”` arrojaría como resultado:

```bash
Hello

World
```

  #### Int o double 💯

Los tipos de datos `int` o `double` son números enteros o decimales. Un número entero puede ser tu edad o la cantidad de integrantes de tu familia. Un número decimal puede ser tu peso o el número de kilómetros que caminas todos los días.

El tipo de dato importa dependiendo de lo que quieras almacenar, por ejemplo, el número 7 puede ser tipo de dato entero o puede ser string:

  - `int = 7`
  - `string = "7"`
  - 7 + 7 = 14
  - “7” + “7” = “77”

  #### Booleano 👩‍⚖️

Los tipos de datos booleanos solo ofrecen dos valores: falso o verdadero. Comúnmente se representan con ceros y unos o con `True` o `False`.

  ### HTML y JavaScript 🖼

JavaScript es un robusto lenguaje de programación que puede ser aplicado a un documento HTML y es usado para crear sitios web dinámicos e interactivos. Mientras JavaScript define la funcionalidad e interactividad de los sitios web, HTML _(HyperText Markup Language)_ sirve como estándar de referencia para la codificación y estructuración de estos, es estático y no tiene alguna función como tal.

Cuando escribes código en HTML estás escribiendo etiquetas HTML. Todas las etiquetas HTML están hechas con un número de partes específicas, incluyendo:

  - El carácter “menor que” `<`.
  - Una palabra o carácter que determina qué etiqueta se está escribiendo.
  - Cualquier número de atributos HTML que se quiera usar, escritos de la forma `nombre=”valor”`.
  - El carácter “mayor que” `>`.

  ### Etiquetas HTML básicas
Hay una serie de etiquetas que son las más usadas para crear cualquier documento HTML:

```html
<body> para el contenido.
<head> para información sobre el documento.
<div> división dentro del contenido.
<a> para enlaces.
<strong> para poner el texto en negrita.
<br> para saltos de línea.
Del <h1> al <h6> para títulos dentro del contenido.
<img> para añadir imágenes al documento.
<ol> para listas ordenadas, <ul> para listas desordenadas y <li> para elementos dentro de la lista.
<p> para parágrafos.
<span> para estilos de una parte del texto.
```

HTML cuenta con un total de 91 etiquetas. Sin embargo, una etiqueta por sí sola a veces no contiene la suficiente información para estar completamente definida. Para ello contamos con los `atributos`: pares de _nombre-valor_ separados por un `“=”` y escritos en la etiqueta inicial después del nombre del elemento. El valor puede estar encerrado entre “comillas dobles” o ‘simples’.

Esta sería la estructura general de una línea de código en lenguaje HTML:

```html
<tag attribute1="value1" attribute2="value2">content</tag>
```

O lo que es lo mismo, con un ejemplo:

```HTML
<a href="http://www.enlace.com" target="_blank">Ejemplo de enlace</a>
```

Donde:

```html
<a> es la etiqueta o tag inicial y </a> la etiqueta de cierre.
href y target son los atributos.
http://www.enlace.com y _blank son las variables.
Ejemplo de enlace es el contenido.
```

Veamos como ejemplo el famosísimo `"Hello, world!"`. Este programa se utiliza generalmente para mostrar en un ejemplo sencillo la sintaxis de un lenguaje de programación y para introducir a los programadores en dicho lenguaje.

Esta es una pieza de código escrito en JavaScript y HTML.

```html
<!DOCTYPE HTML>
<html>

<body>

  <p>Before the script...</p>

  <script>
    alert( 'Hello, world!' );
  </script>

  <p>...After the script.</p>

</body>

</html>
```

Las etiquetas de apertura `<html>` y `<body>` son las que comienzan la estructura de este programa.

```html
<!DOCTYPE HTML>
<html> <------ Inicio de la etiqueta HTML

<body> <------ Inicio de la etiqueta del body del sitio web
```

La etiqueta `<p>` es de inicio de un párrafo y la etiqueta `</p>` da cierre a ese párrafo. La etiqueta `<script>` indica que lo que se escriba dentro de ella corresponde al lenguaje de programación JavaScript, también tiene su etiqueta de inicio y etiqueta de cierre.

```html
<p>Before the script...</p> <------ Un párrafo

  <script> <------ Inicio de la etiqueta de JavaScript
    alert( 'Hello, world!' ); <------ Mensaje en formato de alerta que se mostrará
  </script> <------ Cierre de la etiqueta de JavaScript 

  <p>...After the script.</p> <------ Otro párrafo
```

Finalmente, `</body>` y `</html>` son las etiquetas de cierre del programa. Si observas, son idénticas a las etiquetas con las que se inició a excepción de que tienen un slash `("/")` como señal de cierre.

```html
</body> <------ Cierre de la etiqueta del body del sitio web

</html> <------ Cierre de la etiqueta HTML
```

  #### Variables 📦

Una variable es como una caja, dentro podemos guardar cosas. Las variables de JavaScript son cajas que solo pueden guardar una cosa a la vez. Se las denomina así porque su contenido puede cambiar en cualquier momento durante el desarrollo del programa, son variables.

Cada variable tiene un nombre, tipo de dato y valor. Algunos ejemplos:

![](https://i.ibb.co/XLGLbkV/varia.webp)

Las variables tienen ciertos lineamientos a seguir dependiendo del lenguaje de programación en el que estén. Los lineamientos más comunes son:

  - Respetar mayúsculas y minúsculas en caso de que el lenguaje de programación sea case-sensitive.

  - Agregar al final de la variable un punto y coma `(;)`.

  - Declarar las variables con la palabra var, no es necesario, aunque es recomendable hacerlo. Ejemplo:

     - `var id;`
     - `var apellidoMaterno;`
     - `var estatusActivo;`

  - Contener solo letras, números, símbolos de peso `($)` y guion bajo `(_)`.

Una vez que tienes las variables, puedes asignarles un valor:

  - `var id = 56;`
  - `var apellidoMaterno = "Salazar";`
  - `var estatusActivo = 1;`

  ### Case-sensitive 🎭

Algo importante en los lenguajes de programación es que existe el término _“case-sensitive”_, que en español sería algo así como “sensibilidad a las mayúsculas y minúsculas”. La mayoría de los lenguajes de programación son case-sensitive y JavaScript no es la excepción. Esto quiere decir que el programa tiene la capacidad para distinguir entre mayúsculas y minúsculas.

Para entenderlo mejor, sería algo así:

```Javascript
ApellidoPaterno

apellidoPaterno

apellidopaterno
```

Aunque digan lo mismo son datos diferentes debido a su capitalización, es decir, a su uso de mayúsculas y minúsculas.

  ### Constantes 🗼

Las constantes son lo contrario a las variables, son datos que no varían su contenido durante la ejecución del programa. Una vez que toma un valor, este se mantendrá fijo. Las reglas de su nombre son las mismas que para crear nombres de variable. Al declarar una constante, se usa el prefijo `const`.

  - `const escuela = "Platzi";`
  - `const edadMinima = 18;`

Para documentar una constante necesitas:

  - `Nombre`
  - `Tipo`
  - `Valor`
  - `Uso`

En ciertos programas hay constantes que deben de ser documentadas, por ejemplo, ciertos mensajes que siempre son lo mismo, mensajes de error o de información. Esta información la obtienes principalmente con el equipo de desarrolladores o pueden estar comentadas directamente en el código.

Este es un ejemplo de constantes documentadas, estas muestran mensajes de error:

![](https://i.ibb.co/BfbLGjT/cons.webp)

**No existe una manera única de documentar constantes**. Sin embargo, las tablas son recomendables para mejor visualización de tu lector.

Como technical writer es probable que en algún momento debas documentar bloques de código o escribir para una audiencia de programadores. Así que aprender a leer código para después documentarlo te servirá mucho para tu carrera como _technical writer_.

# 5. Estándares de documentación de código

  ## Cómo documentar una función de código

Ahora que tienes ubicados los conceptos básicos de programación, vamos a documentar una función. Una función normalmente cuenta con cada uno de los conceptos que vimos en una clase anterior: diferentes tipos de datos, variables y operadores.

  ### ¿Qué es una función? 📦

**Una función es un conjunto de procedimientos encapsulados en un bloque de código,** usualmente reciben parámetros, cuyos valores se utilizan para efectuar tareas específicas y adicionalmente retornan un valor.

Si eres programadora o _technical writer_, es importante saber documentar bloques de código. Esta actividad añade suficiente información para explicar lo que hace el código punto por punto, de forma que no solo las computadoras sepan qué hacer, sino que los humanos también lo entiendan (y por qué).

En el caso de las funciones, es vital documentarlas para saber cuál es la tarea que realizan, cuáles son los parámetros que reciben y qué es lo que nos devuelven.

Hay un mito que circula dentro del círculo de programadores: si tú escribes tu código claramente, usas una estructura apropiada y convenciones de nomenclatura adecuadas, con eso es suficiente para llamarle auto-documentación. Claramente **no es cierto**, solamente contribuye en gran medida a que el código sea **fácil de leer**, pero **no significa que esté documentado**.

Cada lenguaje de programación maneja diferentes estándares para documentar, en los ejemplos que hemos visto en este curso nos hemos basado en JavaScript, así que seguiré por esa línea.

JavaScript tiene un formato estándar para la documentación llamado [JSDoc](https://jsdoc.app). Para el caso de las funciones te presento los siguientes formatos.

  ### Comentarios multilínea en cabeceras de funciones 💆🏼‍♀️

Para comentar lo que hace una función usaremos comentarios de este tipo:

  - El comentario comienza con una barra y dos asteriscos (/**).
  - Cada nueva línea lleva un asterisco al comienzo (*).
  - Escribe una descripción al principio para describir la función.
  - La descripción de un parámetro se hace comenzando con @param.
  - Si una línea es demasiado larga, la dividimos en varias líneas. La primera estará sin indentar y el resto indentada (desplazada hacia dentro) respecto a la primera.
  - El comentario cierra con un asterisco y una barra diagonal (*/).
  - Los comentarios puntuales sobre una línea de código se harán con dos barras inclinadas (//).

Si seguimos esa regla, la estructura quedaría así:

```JavaScript
/**

* [algunaFuncion descripción]

* @param {[tipoDeDato]} nombredeParametro1 [descripción]

* @param {[tipoDeDato]} nombredeParametro2 [descripción]

* @return {[tipoDeDato]} [descripción]

*/

var algunaFuncion = function (nombredeParametro1, nombredeParametro2) {

// Hace algo...

};
```
Este es otro ejemplo de este mismo formato aplicado a una función real:

```Javascript
/**

* Agregar dos números y sumarlos

* @param {number} num1 El primer número

* @param {number} num2 El segundo número

* @return {number} El total de los dos números ingresados

*/

var sumarDosNumeros = function (num1, num2) {

return num1 + num2;

};
```

  ### Usa etiquetas normalizadas 🕺

Cuando queramos agregar una descripción de un elemento usaremos etiquetas normalizadas. Por ejemplo, una etiqueta normalizada para describir un parámetro es @param. Siguiendo esa norma, tiene que escribirse tal cual, no puede ser normalizada si usamos @Parametro, @parametros o variaciones por el estilo.

Las etiquetas normalizadas más comunes son:

![](https://i.ibb.co/W0ZBTKd/parm.webp)

  ### Sobre documentar vs. sub documentar: ¿añado información adicional? 👽

Usar el formato estándar JSDoc nos da una visión general de la función principal. Algunas veces con eso es suficiente, nuestra función `sumarDosNumeros()`, por ejemplo, está completamente descrita por el encabezado “Agregar dos números y sumarlos”.

Para funciones ligeramente más grandes es útil añadir comentarios de una línea (o a veces de varias) dentro de nuestra función para describir todo lo que está sucediendo.

Esto puede parecer exagerado para algunos programadores, pero tu yo del futuro siempre le agradecerá a tu yo del pasado por hacer esto cuando vuelvas a un proyecto que no has tocado en algún tiempo. Nunca te preguntarás qué hace una línea de código o por qué la escribiste. Definitivamente es preferible tener código sobre-documentado que sub-documentado.

Aquí un ejemplo de otra función, ¿podrías decirme qué dice cada línea?

```Javascript
/**

* Cambiar la visibilidad de una pestaña de contenido

* @param {string} selector Selector para el elemento

* @param {node} toggle El elemento que disparó la pestaña

*/

var toggleVisibility = function (selector, toggle) {

if (!selector) return;

var elem = document.querySelector(selector);

if (!elem) return;

elem.classList.add('active');

if (toggle) {

toggle.classList.add('active');

}

elem.focus()

if (document.activeElement.matches(selector)) return;

elem.setAttribute('tabindex', '-1');

elem.focus();

};
```

Si estás familiarizado con el código, quizás podrías entenderlo. Pero, si no, te tomaría algo de tiempo hacerlo. Aquí un ejemplo con algo de sobre-documentación:

```Javascript

/**

* Cambiar la visibilidad de una pestaña de contenido

* @param {string} selector Selector para el elemento

* @param {node} toggle El elemento que dispara la pestaña

*/

var toggleVisibility = function (selector, toggle) {

  

// Si no hay un selector, termina

if (!selector) return;

  

// Consigue que se muestre la pestaña

var elem = document.querySelector(selector);

if (!elem) return;

  

// Muestra el elemento

elem.classList.add('active');

  

// Si un elemento toggle fue dado, agrega un .active class para el estilo

if (toggle) {

toggle.classList.add('active');

}

  

// Trae el nuevo elemento dentro de focus

elem.focus()

  

// Si elem.focus() no funciona, agrega tabindex="-1" e intenta nuevamente

// (los elementos que no son focus por defecto necesitan una tabindex)

if (document.activeElement.matches(selector)) return;

elem.setAttribute('tabindex', '-1');

elem.focus();

  
};

```

[PEP 8 -- Style Guide for Python Code | Python](https://www.python.org/dev/peps/pep-0008/)

  ## Buenas prácticas de legibilidad para código y comentarios

En esta clase aprenderemos a documentar **código de muestra** de una manera legible. El objetivo es proporcionar calidad y coherencia en toda tu documentación, **no funcionalidad**. El código de muestra sirve como un mini-portal al contenido de una documentación completa.

Los programadores utilizan el código de muestra (como documentación) para iniciarse rápidamente en el uso de la herramienta y casi siempre navegan por el código antes de leer un tema en su totalidad. Debido a su especial consideración, el código de muestra requiere mucha atención.

Por otra parte, un **bloque de código** es una pieza de un código de muestra, posiblemente de una o unas pocas líneas, como la función que documentamos en una clase anterior. La documentación de bloques de código con el tiempo termina por difuminarse, ya que los equipos no suelen probar los bloques de código con el mismo rigor que los códigos de muestra completos, así que, por esta razón, nos enfocaremos más en este último concepto.

  ### Creando un código de muestra correcto 🏄‍♂️

Un buen **código de muestra** es frecuentemente la mejor documentación para los programadores. No importa si los párrafos y listas de tu documentación son claros, los programadores prefieren mil veces un buen código de muestra. Después de todo, el texto y el código son lenguajes diferentes y es el código de muestra lo que en última instancia le importa al lector.

Al documentar código de muestra debemos de **ser correctos y concisos** para que nuestros lectores lo entiendan rápidamente y lo reutilicen fácilmente sin tener efectos secundarios. Para ello, el código de muestra debe cumplir las siguientes características:

  - Estar programado sin errores.
  - Realizar la función que dice realizar.
  - Estar listo para producción (por ejemplo, el código no debe contener vulnerabilidades de seguridad).
  - Seguir los formatos estándar del lenguaje de programación.

El código de muestra es una oportunidad para influir directamente en la forma en la que los programadores están escribiendo el código. Por lo tanto, el código establecerá parte de la calidad del software. Si eres programadora, considera los pros y contras en caso de que tengas más de una forma de programar una función. Codifica sobre la decisión que sea la mejor y la más legible.

Un **bloque de código** es una pieza de un código de muestra, posiblemente de una o unas pocas líneas de largo, como la función que documentamos en la clase “Documentando una función”. La documentación de bloques de código con el tiempo termina por difuminarse porque los equipos no suelen probar los bloques de código con el mismo rigor que los códigos de muestra completos.

Averigua cuál es el caso primario de lo que el código de muestra debe hacer. Habla con los programadores y directores del proyecto, lee los requisitos y los comentarios del código.

Siempre prueba el código de muestra. Los sistemas adquieren mantenimiento, por lo que el código fuente puede cambiar sobre la marcha y el código muestra dejará de funcionar. Muchos equipos reciclan sus pruebas unitarias y los toman como código de muestra, eso es una mala idea. **El objetivo principal de una prueba unitaria es probar; el único objetivo de un código de muestra es educar al usuario.**

  ### Código ejecutable 🚀

Cuando documentas el código de muestra debes explicar cómo ejecutarlo. Es posible que en el documento tengas que indicar a los usuarios que realicen actividades previas antes de ejecutar el código de muestra, como las siguientes:

  - Descargar archivos.
  - Instalar librerías.
  - Ajustar los valores asignados a las variables.
  - Configurar el entorno de desarrollo integrado (IDE).
  - Ejecutar comandos.

Pero no siempre los usuarios realizan correctamente las actividades anteriores. En algunas situaciones los usuarios prefieren ejecutar código de muestra directamente antes que leer la documentación. Y justo cuando algo no les es comprensible, ahora sí, pasan a leer la documentación.

Esto me recuerda un estudio realizado por Acens, una empresa de tecnologías cloud, que muestra que el 75% de los usuarios reconoce que no lee el manual de instrucciones de los equipos y los dispositivos que adquieren hasta que se les atraviesa una falla.

Recuerda tomar capturas de pantalla del código en acción. Suelen ser útiles para dar a los lectores una idea del objetivo del código de muestra y de lo que la herramienta puede hacer.

  ### Código conciso y comprensible 🤓

El código de la muestra debe ser conciso, lo ideal es que incluyas solo los componentes esenciales. El código irrelevante puede distraer y confundir a tu lector. Recuerda que esta sugerencia no es sinónimo para que los programadores o tú hagan malas prácticas para acortar el código.

Un código de muestra debe de ser comprensible, por lo que es importante contar con las siguientes características:

  - Tener nombres descriptivos de clases, métodos, funciones y variables.
  - Ser un código fácil de descifrar.
  - En caso de que sea un código profundamente anidado, utilizar fuentes en negrita o de color para llamar la atención del lector sobre una sección específica del código de muestra.

  ### Código comentado 📋

Considera las siguientes recomendaciones sobre los comentarios en el código de muestra:

  - Mantén los comentarios cortos, anteponiendo la claridad a la brevedad.
  - Evita escribir comentarios sobre código que es muy predecible en su descripción o, dicho de otra manera, que sea “bastante obvio”. Siempre tomando en cuenta que en algunas ocasiones lo que para nosotros es obvio, quizás para alguien principiante no lo sea. Pensemos en nuestros lectores.
  - Descarga toda tu energía escribiendo comentarios en puntos no intuitivos del código de muestra.
  - Si tus lectores tienen mucha experiencia con una tecnología, no expliques lo que el código está haciendo, en este caso, explica por qué el código lo está haciendo.

En la medida de lo posible ten todo el código necesario en un solo bloque para que los lectores puedan copiar y pegar fácilmente. Ten en cuenta que este tipo de lectores no solo recogen el código, también cualquier comentario insertado.

Los comentarios dentro del código son un gran medio de comunicación técnica. Por lo tanto, pon cualquier descripción que pertenezca al código pegado dentro de los comentarios del código. Por el contrario, cuando tengas que explicar un concepto extenso o complicado pon el texto antes del programa de ejemplo.

Tal vez lo más importante es que te veas a ti mismo como el defensor o representante de aquellos desarrolladores que necesitan usar la API o la herramienta que estás ilustrando. Piensa en las preguntas que te harían si tuvieran acceso a las herramientas que tú usas y pon las respuestas en la documentación + el código de muestra.

  ## Código reutilizable ♻️

Para que tu lector pueda reutilizar fácilmente el código de muestra solicita a las programadoras lo siguiente:

  - Toda la información necesaria para ejecutar el código de muestra, incluyendo cualquier dependencia y configuración.
  - Código que puede ser ampliado o personalizado de manera útil por otros usuarios.
  - Tener un código de muestra fácil de entender, que sea conciso y que compile es un gran comienzo. Si truena la aplicación cuando el lector la esté ejecutando, no estará nada contento. Por lo tanto, cuando describas un código de muestra, ten en cuenta los posibles efectos secundarios. Nadie quiere un código inseguro o extremadamente ineficiente.

  ### El ejemplo y el anti-ejemplo 🙅‍♂️

Además de mostrar a los lectores lo que deben hacer con el código de muestra, a veces es prudente **mostrar a los lectores lo que NO deben hacer**.

Por ejemplo, algunos lenguajes de programación permiten a los programadores poner punto y coma (;) al final de cada instrucción. Supongamos que estás escribiendo un tutorial en un lenguaje como JavaScript que no sugiere poner punto y coma al final de cada instrucción. En este caso, mostrar tanto un buen ejemplo como un anti-ejemplo beneficiará al lector. Por ejemplo:

Esta es una función válida con punto y coma al final de la instrucción.

```Javascript
console.log(getName());
function getName(){
   return  {
     name: ‘@davidenq’;
   }
}
```

Y, en cambio, esta es una función inválida porque cuando ejecutemos el código obtendremos como resultado `undefined` cuando lo que esperamos como resultado es `‘@davidenq’`.

```javascript
console.log(getName());
function getName(){
   return  {
     name: ‘@davidenq’
   };
}
```

  ### Código secuenciado 🧗‍♀️

Un buen conjunto de código de muestra puede incrementar el rango de complejidad de su lectura. Los lectores que no están familiarizados con cierta tecnología normalmente necesitan ejemplos simples para empezar.

El primer y más básico ejemplo en un conjunto de código de muestra se suele denominar 

```python
"Hello, World!"
```

Después de dominar lo básico, los programadores quieren ejemplos más retadores. Así que para que tu documentación sea un buen conjunto de código de muestra, proporciona una buena secuencia de códigos de muestra que vayan subiendo de nivel, desde simples, moderados hasta más complejos.

- [Extendion | Salesforce Documenter](https://marketplace.visualstudio.com/items?itemName=HugoOM.sfdx-autoheader)

# 6. Organización y revisión de tu documentación

  ## Organiza y define el alcance de tus documentos

Ya sabes escribir frases coherentes, puedes escribir párrafos claros y precisos. Ahora vamos a aprender a organizar todos esos párrafos en un documento coherente. Así que… _here we go!_

  ### Define el alcance de tu documento 📏

Un buen documento comienza por definir su alcance. Por ejemplo:

```javascript
Este documento describe cómo crear apps para Android utilizando la API del marco de trabajo de Android y otras bibliotecas de código abierto.
```

Estas definiciones de alcance benefician tanto al lector como al technical writer. Puede suceder que, mientras escribes, el contenido de tu documento se aleje de la definición del alcance. Lo que debes de hacer es **reenfocar el documento al alcance definido** o modificar la definición del alcance.

De cualquier manera, al revisar tu primer borrador puedes eliminar (o pasar a otro documento) cualquier sección que no ayude a satisfacer el alcance del documento.

  ### Define a tu audiencia 👩‍👩‍👧‍👦

Un buen documento especifica explícitamente su audiencia y cualquier conocimiento o experiencia previa. Por ejemplo:

  ```bash
  Esta documentación está dirigida para los desarrolladores familiarizados con la programación en JavaScript y conocimientos de programación orientada a objetos.
  ```

  > Define correctamente tu Buyer Persona

  ### Establece los puntos clave por adelantado ✊

Nuestros lectores son personas ocupadas que no necesariamente leerán todas las páginas de nuestro documento. Imagina que solo pueden leer el primer párrafo de la página uno. Al revisar tu documentación **asegúrate de que el inicio de tu documento responda a las preguntas esenciales de tus lectores**.

Los escritores profesionales concentran una energía considerable en la página uno para aumentar las probabilidades de que los lectores lleguen a la página dos. Sin embargo, la página uno de cualquier documento largo es la más difícil de escribir. Prepárate para revisar la página uno muchas veces.

Siempre **escribe un resumen ejecutivo** (también conocido como abstract) para los documentos largos. Aunque el resumen ejecutivo debe ser muy corto, toma en cuenta que pasarás mucho tiempo escribiéndolo. Un resumen ejecutivo aburrido o confuso es una señal que advierte a los potenciales lectores de que se mantengan alejados.

  ### Cómo escribir para tu audiencia 🚀

A lo largo de este curso he enfatizado varias veces **la importancia de definir tu audiencia**. En esta sección nos centramos en la definición de la audiencia como un medio para organizar tu documento.

Preguntas para definir a tu audiencia ❓
Responder a las siguientes preguntas te ayudará a determinar qué debe contener tu documento:

  - ¿Quién es tu público objetivo?
  - ¿Qué es lo que tus lectores ya saben antes de haber leído el documento?
  - ¿Qué deberían saber o poder hacer tus lectores después de haber leído el documento?

Por ejemplo, supongamos que en una organización implementarán un nuevo algoritmo de redes neuronales profundas en un programa. La siguiente lista contiene algunas respuestas potenciales a las preguntas anteriores:

  - Mi público objetivo está formado por científicas de datos y programadores de mi organización.
  - La mayoría de mi público objetivo estudió conceptos avanzados de programación, conoce la API de Keras y tienen conocimiento intermedio en Python. Sin embargo, alrededor del 25% de mi público objetivo no ha implementado un algoritmo de redes neuronales profundas directamente en un programa.
  - Después de leer este documento:
    
    1. Los lectores tendrán una idea clara de los conceptos teóricos implicados en el desarrollo de software inteligente mediante el uso de redes neuronales profundas.
    2. Los lectores conocerán el alcance y las mejores prácticas en el desarrollo de sistemas predictivos usando el paradigma de aprendizaje profundo, así como los beneficios implicados en su desarrollo.
    3. Los lectores serán capaces de desarrollar soluciones para problemas de alta complejidad mediante el diseño, construcción, validación e implementación de modelos de redes neuronales de alto desempeño.

  ### Cómo organizar la estructura de tu documento 🏗

Después de definir tu público, organiza el documento para proporcionar lo que los lectores deben saber o ser capaces de hacer después de leer el documento. Por ejemplo, el esquema del documento podría ser el siguiente:

  1. Introducción a la Inteligencia Artificial y al Aprendizaje Profundo
    1.1. Entendiendo las Redes Neuronales y el Aprendizaje Profundo.
    1.2. Desarrollo de Redes Neuronales en Python.
  2. Fundamentos de Aprendizaje de Máquina
    2.1. Ramas de Aprendizaje de Máquina.
    2.2. Librerías para Aprendizaje de Máquina en Python.
  3. Variantes de Redes Neuronales
    3.1. Variantes de Redes Neuronales y sus Fundamentos.
    3.2. Arquitectura de una Red Neuronal de Visión Artificial.
  4. Ejemplos de Aplicaciones Modernas usando Aprendizaje Profundo
    4.1. Generación de Textos Usando Redes Neuronales Recurrentes.
    4.2. Modelos de redes neuronales para generar datos.

Además, ten en cuenta la segmentación de tu público para darle un enfoque correcto a tu documento. Por ejemplo, el público objetivo estudió Python, pero aproximadamente una cuarta parte de tu público podría no recordar los detalles de la API de Keras. Por lo tanto, es probable que en el documento insertes referencias a documentación externa de la API en lugar de intentar explicarla.

  ### Documentación modular: divide tu tema en secciones 🔪

La programación modular se divide en archivos, clases y métodos. El código modular es más fácil de leer, entender, mantener y reutilizar. Hacer que nuestros documentos sean modulares nos da los mismos beneficios. Si eres programador, probablemente tengas una fuerte familiarización con la modularidad funcional en el código, pero ¿cómo usarías estos principios en tu escritura?

Imagina que tienes una bodega vacía en la que necesitas guardar un lote de cajas grandes, medianas y pequeñas. ¿Cómo planearías meter las cajas para asegurarte de que todas podrán entrar en la bodega? Por supuesto que primero meterías las cajas grandes, luego las medianas y finalmente las pequeñas. Si intentaras hacer esto en el orden contrario, fallarás.

La cabeza del lector se parece mucho a una bodega vacía. La información generalmente se acomoda en su cerebro en tres tamaños: grande, mediana y pequeña. Los tamaños de la caja son el tamaño de la información. Necesitas acomodar cada caja dentro del espacio de la cabeza de nuestro lector para que procese y entienda nuestros escritos.

Para decidir el orden de esta información y saber el orden en que debemos de escribirla puedes usar la estrategia de grabarte a ti mismo hablando (o escribiendo libremente) sobre algún tema durante un corto período de tiempo, pueden ser 2 a 5 minutos. Sé específico en tu explicación. Inténtalo varias veces hasta que seas capaz de entenderte a ti mismo y te parezca coherente.

Lo sé, esto requiere disciplina, pero es una herramienta infalible para comunicar la información de manera modular.

  ### ¿Pondrás en práctica estos ejercicios? 🔥

Antes de avanzar a la siguiente clase debes completar los siguientes 2 desafíos.

1️⃣ Escribe una documentación para principiantes sobre cómo conectar un celular vía USB a tu computadora.

2️⃣ Escribe la documentación de un programa o función que hayas escrito por tu propia cuenta (en cualquier lenguaje de programación). Imagina que quieres convertir tu proyecto en open-source y estás escribiendo esta documentación para que otras programadoras entiendan tu programa y puedan ayudarte a escalarlo.

Recuerda poner en práctica lo que aprendiste en esta clase. Debes definir el alcance, audiencia y puntos clave de tus documentos.

Reto 1
### Como conectar un celular via USB a tu computadora
  #### Requisitos
1. Celular smartphone con entrada
  - Tipo C o
  - Tipo OTG
2. Cable compatible con tu celular
  - Tipo C a USB o
  - Tipo OTG a USB
3. Computadora con entrada USB

#### Pasos para conectar

  1. Prende la computadora
  2. Verifica que el celular tenga suficiente bateria, de preferencia arriba de 30%
  3. Conecta la entrada Tipo C o OTG al celular
  4. Conecta el puerto USB a la computadora
  5. Revisa que en tu computadora te indique que se encontro dispositivo

Reto 2

```javascript
/**
 * @description       : Servicio REST que envia información a pagina web
 * @author            : erika.villanueva@salauno.com.mx
 * @group             : Digital Grow
 * @last modified on  : 23-01-2021 
 * @last modified by  : erika.villanueva@salauno.com.mx
 * Modifications Log
 * Ver   Date         Author                               Modification
 * 1.0   01-22-2021   erika.villanueva@salauno.com.mx   Initial Version
**/

@RestResource(urlMapping ='/Service_Website')
global with sharing classService_Website{

    @HttpPost
    global staticvoidService_WebsiteLoginPacientesGet(){
        // Obtiene el JSON recibido por el servicio POST
        RestRequest rest_Request = RestContext.request;
        String respuesta_Body = rest_Request.requestBody.toString();

        // Convierte Json recibido a Wrapper   
        Wrapper_PeticionWeb wrp_PeticionWeb = (Wrapper_PeticionWeb) JSON.deserialize(respuesta_Body,Wrapper_PeticionWeb.class);

        // Obtiene la información solicitada por servicio POST
        SObject resultado = get_InformacionSolicitada(wrp_PeticionWeb);
        
        envia_Resultados(resultado);
    }

    // Envia respuesta a servicio POST
    privatestaticvoidenvia_Resultados(SObject resultado){
        RestResponse respuesta = RestContext.response;
        if (respuesta == null) {
            respuesta = new RestResponse();
            RestContext.response = respuesta;
        }
        respuesta.responseBody = Blob.valueOf(JSON.serialize(resultado));
    }

    // Dependiendo los parametros recibidos por el servicio POST obtiene la información a enviar
    privatestatic SObject get_InformacionSolicitada(Wrapper_PeticionWeb wrp_PeticionWeb){
        String str_Query = 'SELECT Id, Name FROM '+ wrp_PeticionWeb.objeto +' WHERE id = '+wrp_PeticionWeb.id_Registro;
        SObject obj_Dinamico = Database.query(str_Query);
        return obj_Dinamico;
    }

    // Valida que el Json recibido contenga las variables correctas
    global classWrapper_PeticionWeb{
        public String id_Registro {get;set;}
        public String objeto {get;set;}
    }
```
  ## Utiliza Markdown en documentos técnicos

**Markdown** es un lenguaje de marcado ligero que muchos profesionales utilizan para **crear y editar documentos técnicos**. Con Markdown escribes el texto en un editor de texto plano insertando caracteres especiales para crear encabezados, negritas, viñetas, etc. El texto plano es simplemente el alfabeto normal, con unos cuantos símbolos familiares como asteriscos ( * ) o comillas simples ( ``` ). El siguiente ejemplo muestra una descripción técnica sencilla formateada con Markdown:

![](https://i.ibb.co/jMsHgcW/mark.webp)


El sitio web [Markdown Tutorial](https://www.markdowntutorial.com) nos comparte que, a diferencia de las aplicaciones de procesadores de texto, el texto escrito en Markdown puede ser fácilmente compartido entre diferentes equipos, dispositivos móviles y personas. Y se está convirtiendo rápidamente en el estándar para escritos de académicos, científicos, escritores y muchas más. Ejemplo de ellos son Platzi, GitHub y Reddit, quienes utilizan Markdown para dar formato a sus comentarios.

  ### ¿En qué se utiliza Markdown? 🤓

De acuerdo con la documentación de Markdown, este lenguaje de marcado es bueno para:

  + Páginas web
  + Contratos
  + Notas
  + Libros
  + Presentaciones
  + Emails
  + Documentación

¡Y muchos más!
Hoy en día muchos escritores técnicos tienen sus blogs, así que cualquiera puede usar Markdown para sus sitios web y blogs.

  ### Editores de Markdown 📋

Para trabajar con Markdown puedes hacerlo desde editores de texto como:

- [Atom](https://atom.io)
- [Sublime Text](https://www.sublimetext.com)
- [Notepad++](https://notepad-plus-plus.org/downloads/)
- [Boost Note](https://boostnote.io)
- [Visual Studio Code](https://code.visualstudio.com)

Solo debes crear un archivo con extensión .markdown o .md. Escribe el texto como lo haces habitualmente pero utilizando la sintaxis de Markdown. A continuación, guarda el archivo y, si tu herramienta admite Markdown, impórtalo.

Si quieres un editor online, te recomiendo:

  - [StackEdit](https://stackedit.io)
  - [Notion](https://www.notion.so)
  - [Zen](https://zen.unit.ms)

Solo abre el sitio y empieza a escribir. La mayoría de estos editores te muestran dos paneles, uno en donde vas escribiendo y otro en donde se va viendo la adaptación a Markdown.

  ### Breve introducción a la sintaxis de Markdown Ⓜ

Dar formato a textos mediante Markdown tiene una curva de aprendizaje suave. Cabe mencionar que Markdown no hace grandes modificaciones como cambiar el tamaño de la fuente, el color o el tipo. Lo único sobre lo que se puede trabajar es en cómo se muestra el texto, por ejemplo, marcar texto en negrita, crear encabezados u organizar listas de elementos. A continuación, te dejo algunos formatos básicos de introducción al Markdown:

  - \# H1
  - \## H2
  - \### H3
  - \#### H4
  - \##### H5
  - \###### H6

  ### 2. Énfasis

Tres básicos elementos en este formato de texto son: itálica, **negrita** y ~~tachada~~. Para hacer una palabra en itálica debes poner un asterisco `(*)` al inicio y al final de cada palabra. Si quieres el formato negrita, serán dos asteriscos `(**)` al inicio y al final de la palabra o frase. Y si es formato tachado, entonces serán dos virgulillas `(~~)` al inicio y final de cada palabra o frase.

  - \*Itálica*
  - \**Negrita**
  - \~~Tachada~~

  - *Itálica*
  - **Negrita**
  - ~~Tachada~~

  ### 3. Listas

Como te mencioné anteriormente, hay varios tipos de listas. Utiliza las listas ordenadas (numeradas) cuando los elementos deban seguir un orden (por ejemplo, instrucciones para cocinar) y las listas desordenadas (con viñetas) cuando no importa el orden de las instrucciones (por ejemplo, una lista de mercado).


- Lista desordenada
- Usa este tipo de listas
- Cuando no importe el orden
- De los elementos de la lista

1. Lista ordenada
2. Aquí los elementos
3. De la lista
4. Debes ir en este orden

  ### 4. Enlaces

Este tipo de enlace es el llamado enlace en línea. Para crear un enlace en línea debes encerrar el texto del enlace entre corchetes `( [ ] )` y después encerrar el enlace entre paréntesis `( ( ) )`.

```shell
[Platzi](https://platzi.com/)
```
[Platzi](https://platzi.com/)

**¿Verdad que es muy sencillo?**

Si quieres profundizar, aquí está la [Sintaxis Básica de Markdown](https://www.markdownguide.org/basic-syntax/) para que te asegures de conocerlo a profundidad. Si lo que quieres es practicar, reserva 20 minutos de tu día a este tutorial de Markdown para aprender toda la sintaxis básica.

### Characters You Can Escape

You can use a backslash to escape the following characters.

| Character	| Name |
| :--------: | :---------: |
| \ |	backslash |
| `	| backtick (see also escaping backticks in code) |
| *	| asterisk |
| _	| underscore |
| { }	| curly braces |
| [ ]	| brackets |
| < >	| angle brackets|
| ( )	| parentheses |
| # |	pound sign |
| +	| plus sign|
| -	| minus sign (hyphen) |
| . |	dot |
| !	| exclamation mark |
|	pipe | (see also escaping pipe in tables) |

  ### Ahora escribe tu documentación con Markdown 👊

Es hora de poner en práctica todo lo que aprendiste en esta clase. ¿Recuerdas la documentación que escribiste en la clase anterior? Ahora debes transformarla a Markdown aprovechando todas las funcionalidades y formatos que acabas de aprender. ¡No olvides dejar tu resultado en los comentarios!

  ## Guía para revisar documentación en equipo de manera efectiva

En esta clase abarcaré consejos de edición que te ayudarán a convertir tu primer borrador en un documento que comunique de manera más clara la información que tu audiencia necesita. Utiliza un consejo o utilízalos todos; lo importante es encontrar **una estrategia que funcione para ti** y luego hacer que esa estrategia forme parte de **tu rutina de escritura**.

  ### Autoedición ✌️

Imagina que acabas de escribir el primer borrador de un documento. ¿Cómo lo mejorarías? En la mayoría de casos, el trabajo hacia un documento final (publicado) es un proceso iterativo. **Transformar una página en blanco en un primer borrador es a menudo el paso más difícil.** Después de escribir un primer borrador asegúrate de reservar suficiente tiempo para perfeccionar tu documento.

- [La guía de los 12 pasos para escribir en internet](https://platzi.com/blog/la-guia-para-escribir-en-internet/)

Adopta una guía de estilo 📚
Una guía de estilo es un conjunto de normas para la redacción y el diseño de contenidos; define el estilo que debe utilizarse en la comunicación dentro de una organización determinada. Las empresas, organizaciones y los grandes proyectos de código abierto suelen redactar o adoptar una **guía de estilo** existente para su documentación.

Para escribir la documentación de una manera más clara y mantener un tono, voz y estilo consistentes en tu documentación puedes leer y seguir una de estas guías de estilo:

  - [A List Apart](https://alistapart.com/about/style-guide/)
  - [Microsoft Style Guide](https://docs.microsoft.com/en-us/style-guide/welcome/)
  - [University of Oxford Style Guide](https://www.ox.ac.uk/sites/files/oxford/media_wysiwyg/University%20of%20Oxford%20Style%20Guide.pdf)
  - [Google Developer Documentation Style Guide](https://developers.google.com/style)
  - [IBM Style Guide](https://www.ibm.com/developerworks/library/styleguidelines/index.html)
  - [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)

  Algunas de estas pautas son puntos destacados que vimos en los módulos de “Introducción al Technical Writing” y “Estructura gramatical”. Puede que recuerdes algunas de las siguientes técnicas:

  - Usar la **voz activa** para dejar claro quién está realizando la acción.
  - Transcribir los pasos secuenciales en **listas numeradas**.
  - **Usar viñetas** en la mayoría de las listas.

Los documentos con contenido efectivo presentan muchas otras técnicas que pueden ser útiles a la hora de escribir documentación técnica, por ejemplo:

  - Escribir en segunda persona. Referirse a su audiencia como “tú” en lugar de “nosotros”.
  - Poner circunstancias particulares antes de una instrucción, en lugar de después, como en los ejemplos de la siguiente imagen.

![](https://i.ibb.co/f4479QJ/comen.webp)

Otro caso de una circunstancia particular puesta antes de una instrucción es:

  - Formatear el texto plano en `formato de código.`

Ejemplos:

```html
- En HTML usa la etiqueta ``.
- En Markdown, usa comillas invertidas (```).
```

  ### Piensa como tu audiencia 🧠

¿Quién es tu público? Retrocede, _sal de la caja_ y trata de leer tu borrador desde el punto de vista de tus lectores. Asegúrate de que el objetivo de tu documento sea preciso y proporciona definiciones para cualquier término o concepto que pueda ser desconocido para tus lectores.

Puede ser útil hacer un “boceto” de una persona para tu público. Un personaje puede consistir en cualquiera de los siguientes atributos:

  - Un rol como ingeniero de TI o un científico de datos.
  - Un objetivo final, implementar un algoritmo de redes neuronales profundas.
  - Un conjunto de suposiciones sobre el personaje y su conocimiento y experiencia. Por ejemplo, podrías asumir que tu personaje está:

    – Familiarizado con Python.
    – Corriendo un sistema operativo Linux.
    – Cómodo siguiendo las instrucciones de la línea de comandos.

Entonces puedes revisar tu borrador con tu personaje en mente. Puede ser especialmente útil contarle a tu audiencia cualquier suposición que hayas hecho. **También puedes proporcionar enlaces a recursos donde puedan aprender más si necesitan repasar un tema específico.**

Ten en cuenta que confiar demasiado en un personaje (o dos) puede dar lugar a un documento demasiado centrado para ser útil a la mayoría de tus lectores.

Para refrescar y obtener más información sobre este tema consulta la clase sobre cómo escribir específicamente para tu audiencia. 😉

  ### Léelo en voz alta 📣  

Dependiendo del contexto, **el estilo de tu escritura puede enganchar,** alienar o incluso aburrir a tu público. El estilo deseado de un documento determinado depende en cierta medida de la audiencia. Por ejemplo, la guía del colaborador de un nuevo proyecto de código abierto destinado a reclutar voluntarios podría adoptar un estilo más informal, mientras que la guía del desarrollador de una aplicación para una empresa comercial podría adoptar un estilo más formal.

Para comprobar que tu escritura es conversacional léela en voz alta. Escúchate y detecta si hay frases incómodas, oraciones demasiado largas o cualquier otra cosa que no parezca natural. También puedes intentar pedirle a otra persona que lea tu borrador en voz alta para ti, de esta manera te será más fácil detectar ajustes a realizar sobre tus escritos.

  ### Vuelve a tu borrador más tarde 🔙

Después de que escribas tu primer borrador (o segundo o tercero), déjalo a un lado. Vuelve a él después de una hora (o dos o tres) y trata de leerlo con ojos frescos. Casi siempre notarás algo que podrías mejorar.

  ### Cambia el contexto 🔁

A algunos escritores les gusta imprimir su documentación y revisar en una copia en papel y con lápiz rojo en la mano. Un cambio de contexto al revisar tu propio trabajo puede ayudarte a encontrar cosas para mejorar. Para una toma moderna de este consejo clásico, copia tu borrador en un documento diferente y cambia la fuente, el tamaño y el color.

  ### Encuentra un editor colega 👥

Así como los ingenieros necesitan colegas para revisar su código, los escritores necesitan editores que les den retroalimentación sobre sus documentos. Pídele a alguien que revise tu documento y te dé una retroalimentación específica y constructiva. Tu editor colega no necesita ser un experto en el tema técnico de tu documento, pero sí necesita estar familiarizado con la guía de estilo que sigues.

  ### Revisa tu documentación y entrégale un gran feedback a tus compañeros 🤝

Para completar esta clase también debes completar 2 nuevos desafíos:

1️⃣ Repasa todo lo que has aprendido en esta clase (y durante todo el curso), devuélvete a las clases anteriores de este módulo y entrégale feedback a los demás estudiantes que compartieron sus desafíos en la sección de comentarios.

2️⃣ Revisa una vez más la documentación de tus últimos retos. Aprovecha para agradecer por el feedback que te han dado tus compañeros y mejora tu documento. Y, por supuesto, compártenos en la sección de comentarios tu nueva documentación (y qué aspectos lograste mejorar gracias a esta nueva revisión).

Sé que entregar y recibir feedback no es nada fácil. De hecho, es una habilidad indispensable a la hora de trabajar en equipo profesionalmente. No te preocupes, lo harás muy bien. Te recomiendo tomar estas clases para asegurarte de hacerlo excelente.

- [Cómo dar y recibir Feedback Efectivo](https://www.youtube.com/watch?v=X0xS5VNM2x0)
- [Sé un buen líder dando feedback constructivo](https://www.youtube.com/watch?v=Y0x1Iu4mpxk)

## Cómo organizar documentos largos

¿Cómo se organiza una gran colección de información en un documento o en un sitio web de manera coherente? ¿Cómo se reorganiza un documento o sitio web desordenado en algo accesible y útil? Son preguntas a las que te enfrentarás una vez que comiences a desarrollar documentos técnicos largos. Pero no todo está perdido, los siguientes procedimientos pueden ayudarte:

  - **Elige escribir un solo documento grande** o un conjunto de documentos.
  - **Organiza** un documento.
  - **Añade** navegación.
  - **Revela** información de manera paulatina.

Veamos más a fondo cada punto a continuación.

### ¿Cuándo escribir documentos largos? 🤔

Hay dos sugerencias para resolver esta pregunta. La primera es que puedes organizar una colección de información en documentos independientes más largos. O puedes organizar un conjunto de documentos interconectados más cortos. Este último suele publicarse normalmente en sitios web, en wikis o en formatos estructurados similares.

Recordemos que en la clase sobre cómo escribir específicamente para tu audiencia aprendimos que nuestra audiencia puede ser variada. Tendremos lectores que responderán positivamente a los documentos más largos y otros lectores que quizás no. Considera los siguientes perfiles de dos lectores imaginarios para los que estás escribiendo la documentación:

  - Laura es muy dispersa, para ella leer documentos largos es difícil y desorientador. Prefiere lo práctico, le gusta usar la herramienta de “búsqueda” en el sitio para encontrar respuestas rápidas a sus preguntas.

  - A Eduardo le encanta leer y se siente a gusto navegando en documentos largos. A menudo utiliza la herramienta de “búsqueda” en su navegador para encontrar información útil.

Entonces, ¿deberías organizar tu material en un solo documento o en un conjunto de documentos en un sitio web? Antes de tomar una decisión, considera los siguientes puntos:

  - Las guías de instrucciones, los resúmenes introductorios y las guías conceptuales suelen funcionar mejor como documentos más breves cuando se dirigen a lectores que son nuevos en el tema. Por ejemplo, un lector que es novato en el tema que estás explicando puede tener complicaciones para recordar bastantes términos y conceptos nuevos. Recuerda que tu público podría estar leyendo tu documentación para obtener una visión rápida y general del tema.

  - Los tutoriales exhaustivos, las guías de mejores prácticas y documentos afines pueden funcionar bien como documentos más extensos, especialmente cuando están dirigidos a lectores que ya tienen alguna experiencia con las herramientas y el tema.

  - Un gran tutorial puede basarse en una narración para guiar al lector a través de una serie de tareas relacionadas en un documento más largo. Sin embargo, esto puede ser cansado para el lector. En este punto puedes aplicar la máxima de “divide y vencerás”, estos tutoriales pueden ser divididos en capítulos y hacer más amena su lectura.

  - La mayoría de los documentos largos no están diseñados para ser leídos en una pasada. Normalmente, como lectores solemos escanear una página de referencia para buscar una explicación directa de lo que necesitamos saber.

  ### Organizar un documento 📃

A continuación, te enseñaré algunas técnicas para planificar un documento más extenso, entre ellas la creación de un esquema y la redacción de una introducción. Después de completar el primer borrador de un documento, puedes revisarlo contra tu esquema e introducción para asegurarte de que no se ha perdido nada del enfoque original que pretendes abarcar.

  **Esbozar un documento ✍**️

Empezar con un **esquema** puede ayudar a agrupar los temas de los que quieres escribir y determinar en dónde se necesitan más detalles. El esquema te ayuda a mover los temas antes de que te pongas a escribir.

Puede ser útil pensar en un esquema como la narrativa de tu documento. **No existen lineamientos a seguir para escribir un esquema**, pero las siguientes sugerencias te proporcionarán consejos prácticos que pueden ser útiles:

- Antes de pedirle al lector que realice una tarea, explícale por qué la hará. Por ejemplo, los siguientes puntos ilustran una sección de un esquema de un tutorial sobre cómo instalar un plugin en WordPress:

  – Conocer cómo instalar un plugin directamente desde WordPress. El lector aprenderá cómo instalarlo de manera directa, manual o usando FTP.
  – Enlistar los pasos para descargar un plugin e instalarlo.

- Limita cada paso de tu esquema a describir un concepto o completar una tarea específica.

- Estructura tu esquema de manera que tu documento presente la información más relevante para tu lector. No satures de información a tu lector de manera instantánea. Por ejemplo, es probable que el lector no necesite o no quiera saber la historia del proyecto en las secciones introductorias de tu documento cuando apenas está empezando con lo básico. Si crees que la historia del proyecto es útil, entonces agrégalo como documentación externa. Adjunta un enlace al final de tu documento que dirija a dicha información.
- Considera la posibilidad de explicar un concepto y luego demostrar cómo el lector puede aplicarlo. Los documentos que alternan entre la información conceptual y actividades prácticas son una forma atractiva de aprender.
- Antes de empezar a redactar, comparte el esquema con tus colaboradores. Los esquemas son especialmente útiles si trabajas con un equipo de colaboradores que van a revisar y poner a prueba tu documento.

  ### Escribe una introducción a tu documento 🥁

Si los lectores de tu documentación no encuentran relevancia en el tema, es probable que lo ignoren. Para establecer las reglas básicas para tus usuarios te recomiendo que proporciones una introducción que incluya la siguiente información:

  - El tema que trata el documento.
  - Qué conocimientos previos, ya sea teóricos o técnicos, se espera que tengan los lectores.
  - Lo que el documento no cubre.

Recuerda que quieres provocar que tu documentación sea fácil de enganchar, así que no intentes cubrir todo en la introducción.

El siguiente párrafo es un ejemplo de una guía para desarrolladores que quieren aprender a crear una app móvil en Android. En este párrafo de introducción muestra las recomendaciones antes mencionadas como una visión general:

```
Bienvenido a las guías para desarrolladores de Android. Los documentos que se indican en el área de navegación de la izquierda te enseñan a crear apps de Android utilizando API del marco de trabajo de Android y otras bibliotecas.

Esta guía es para perfiles técnicos con conocimiento intermedio en el lenguaje de programación Java. Si eres nuevo en Android y quieres entrar en el mundo de la programación, empieza con el instructivo *Crea tu primera app*.
```

Después de completar el primer borrador, verifica que todo el documento tenga las expectativas que estableciste en la introducción. ¿Proyectas en tu introducción una visión general precisa de los temas que cubre tu documento? Si la respuesta es sí, es señal de que vamos bien, si es lo contrario, despeja un rato tu mente y vuelve a redactarla.

  ### Añade navegación en tu documento 🚀

Cuando estás creando o editando un documento largo, probablemente tienes que añadir navegación. Puede sonar como una tarea larga, pero afortunadamente puedes hacerlo con solo algunos clics.

Proporcionar navegación y señalización a tus lectores asegura que puedan encontrar lo que buscan y la información que necesitan al alcance. La navegación clara incluye:

  - Secciones de introducción y resumen.
  - Desarrollo claro y lógico del tema.
  - Títulos y subtítulos que ayudan a los usuarios a comprender el tema.
  - Menú de índice que muestra a los usuarios dónde se encuentran en el documento.
  - Enlaces a recursos relacionados o a información más detallada.

Los consejos de las siguientes secciones pueden ayudarte a planificar los encabezados de tu documentación.

  ### Elige los encabezados basados en tareas 👩‍💻

Elige un nombre de encabezado que describa la tarea en la que está trabajando tu lector. Evita los encabezados que se basen en terminologías o herramientas desconocidas.

Por ejemplo, supongamos que estás documentando el proceso de ejecución de una app móvil en un dispositivo real. Para ejecutar la app el lector debe conectar el dispositivo a la computadora con un cable USB y habilitar la depuración. Posteriormente, ejecutar la app desde Android Studio. A primera vista podría parecer lógico añadir cualquiera de los siguientes encabezados a las instrucciones:

  - Habilitar depuración usando USB.
  - Ejecutar Android Studio.

A menos que tus lectores ya tengan mucha experiencia con la terminología y los conceptos de este tema, estarían perfectos esos encabezados. Sin embargo, es preferible usar términos más familiares, por ejemplo, _“Cómo ejecutar la app desde un dispositivo real”_.

  ### Agrega una breve introducción bajo cada encabezado 📝

La mayoría de los lectores agradecen al menos una breve introducción bajo cada encabezado para proporcionar algún contexto. Evita poner un encabezado de nivel tres inmediatamente después de un encabezado de nivel dos. No lo hagas así:

  - \## Cómo ejecutar una app
  - \### Cómo ejecutar la app en un dispositivo real

  **Mejor hazlo así:**

\## Cómo ejecutar una app

En la lección anterior, aprendiste a crear una app para Android que muestra el mensaje "Hello, World!". Ahora podrás ejecutar la app en un dispositivo real o en un emulador.

\### Cómo ejecutar una app móvil

  ### Revela información paulatinamente 🗣

El aprendizaje de nuevos conceptos, ideas y técnicas puede ser una experiencia gratificante para muchos lectores que se sienten cómodos leyendo la documentación a su propio ritmo. Sin embargo, enfrentarse a demasiados nuevos conceptos e instrucciones demasiado rápido puede ser abrumador. Es más probable que los lectores sean receptivos a documentos más largos que les revelen paulatinamente nueva información cuando la necesiten.

Las siguientes recomendaciones te ayudarán a implementar la información de una manera paulatina en tus documentos:

  - En la medida de lo posible, agrega nueva terminología y conceptos.
  - Introduce tablas, diagramas, listas y encabezados cuando sea apropiado.
  - Reorganiza listas largas en listas cortas que expliquen cómo completar las subtareas.
  - Empieza con ejemplos e instrucciones sencillas y añade paulatinamente técnicas más interesantes y complicadas. Por ejemplo, en un tutorial para el uso de un cliente de correo electrónico comienza explicando cómo personalizar su bandeja, cómo enviar correos electrónicos y luego introduce otras técnicas como organizar los mensajes por medio de etiquetas.

# 7. Diseño de documentos

  ## Crea ilustraciones instructivas

El uso de ilustraciones instructivas es una fase fundamental durante la elaboración de la documentación técnica. **Las ilustraciones ayudan a transmitir a tus lectores información compleja de una manera más comprensible y clara**. Un caso recurrente que sucede cuando se trata de leer material técnico es que los lectores prefieren ver ilustraciones en lugar de texto. Estas se utilizan para informar, explicar, ilustrar o aprender. Y pueden ser imágenes, diseños, gráficos instructivos, entre otros.

En este clase aprenderás algunas técnicas y recursos de ayuda para crear ilustraciones instructivas que ayuden a que tus lectores logren comprender la información expuesta en tus documentos de una manera más sencilla. Como dice el proverbio chino: _"una imagen vale más que mil palabras"_.

  ### Enfoca tu ilustración en el pie de imagen 👩‍🎨

En ocasiones será complicado elegir la ilustración ideal para mostrar a tus lectores. Te sugiero que comiences por escribir _primero el pie de imagen_ (o pie de foto/ilustración…) y a partir de eso tendrás más enfoque en lo que debe de incluir tu ilustración. _El pie de imagen es el breve texto descriptivo que aparece al borde inferior de la ilustración y tiene como objetivo darle un sentido concreto a lo que se muestra_.

Las características de un buen pie de imagen son las siguientes:

  - Es _breve y conciso_.
  - Aporta información necesaria.
  - Debe _atraer la atención_ de tu lector.

El siguiente pie de imagen es un ejemplo del libro “Blockchain for Dummies” de IBM. Como observarás, la descripción del pie de imagen tiene que ver con lo que se muestra en la ilustración:

![](https://i.ibb.co/8YFTbNz/pie.webp)

  ### Evita ilustraciones técnicas complejas 🚫

Así como la sugerencia de escribir pies de imágenes breves y concisos, lo mismo pasa con el contenido de las ilustraciones que vayas a usar. Deben de ser simples y legibles a la vista. Los technical writers de Google recomiendan evitar las ilustraciones que requieren más de cinco puntos para explicarse. Por ejemplo, este tipo de ilustraciones podrían llegar a confundir a tus lectores:

![](https://i.ibb.co/ZSMD2TB/comp.webp)

Ilustraciones complejas

En estos casos, la sugerencia que los expertos dan es que se divida en módulos para que la ilustración sea visualmente digerible. La siguiente ilustración, por ejemplo, pasa de ser un sistema complejo a un sistema dividido en tres módulos:

![](https://i.ibb.co/5FFdP1C/modulos.webp)

Ilustración dividida en módulos

  ### Usa señales visuales 👀

Cuando realices capturas de pantallas (screenshots) con varios elementos utiliza figuras para señalar el punto a observar. Puedes usar un rectángulo, un óvalo, una flecha o cualquier figura que enfoque la atención de tu lector para que no se pierda en la imagen.

![](https://i.ibb.co/s9Fb8FV/marca.webp)

Captura de pantalla sin señal visual

![](https://i.ibb.co/Bz8hbzr/marca1.webp)

Captura de pantalla sin señal visual

  ### Simplifica tus ilustraciones 🏳

¿Recuerdas que cuando escribimos nuestros primeros documentos tenemos que hacerlos a manera de borradores y sobre la marcha irlos mejorando? Es lo mismo con las ilustraciones. Podemos modificar las ya existentes y, por supuesto, irlas mejorando.

Cuando hagas la revisión de tus ilustraciones, toma en cuenta los siguientes puntos:

  - Hacer lo más simple posible la ilustración.
  - Dividir en módulos la ilustración en caso de que sea compleja a la vista.
  - Revisar que los colores y las fuentes sean adecuadas para su lectura.

Un ejemplo sencillo podría ser este mapa del metro de la CDMX. Imaginemos que el punto principal es mostrar las rutas del metro. Evidentemente, el segundo ejemplo es más legible que el primero.

![](https://i.ibb.co/hsyhq0d/ilus.jpg)

Metro de la CDMX con trasfondo de las avenidas principales

![](https://i.ibb.co/nnd1mPg/ilus1.webp)

Metro de la CDMX sin trasfondo de las avenidas principales

  ### Herramientas gratuitas ⭐

Te comparto una pequeña lista de herramientas gratuitas de diagramado que puedes encontrar en la web, son gratuitas y muy intuitivas:

  -  [Draw.io](https://app.diagrams.net)
  -  [Lucidchart](https://www.lucidchart.com/pages/)
  -  [Creately](https://creately.com/es/home/)
  -  [Miro](https://miro.com)

**Nunca pierdas de vista el objetivo de tus documentos técnicos ni las necesidades de tus lectores.** Mientras construyes tus documentos ten siempre en mente a tu audiencia. De esta manera, con la práctica y experiencia que obtengas a lo largo de tu trayectoria lograrás crear una documentación técnica eficiente y sencilla.

**Conviértete en el superhéroe que los lectores técnicos necesitan.**

# 8. Conclusiones

  ## Siguientes pasos para convertirte en Technical Writer profesional

Hemos llegado al final del __Curso de Introducción al Technical Writing y Documentación de Código__. Me da mucho gusto que hayas llegado hasta aquí. Puedes estar segura de que todo lo que hemos aprendido será impresionantemente útil para tu presente y futuro profesional.

Recuerda lo que dice Google acerca de que los technical writers: __somos híbridos raros que poseemos una mezcla poco común de talentos__. ¿Qué esperas para explotar todo tu talento y aplicar lo aprendido en tu día a día? ¡Sorprende al mundo con tus habilidades!

### 🤓 Hasta este punto debes tener muy claros los siguientes puntos:

  - Technical Writing va mucho más allá de la creación de manuales para software y computadoras. Nuestro objetivo es transformar la información técnica en un lenguaje fácilmente comprensible para tu audiencia.
  - Sabes identificar a la audiencia para la cual van dirigidos los documentos técnicos que realices.
Conoces herramientas y recomendaciones a considerar cuando recopilas información para tus documentos técnicos.
  - Puedes aplicar los conceptos de gramática básica en tus escritos.
  - Aprendiste a usar términos adecuados al definir terminología desconocida.
  - Escribes tu documentación de una manera corta, correcta, efectiva y principalmente con claridad.
  - Conviertes los textos en prosa a listas y/o tablas con información útil para el lector.
  - Estructuras párrafos claros y precisos para dar definiciones, explicar un proceso o presentar argumentos.
  - Repasaste o diste un primer vistazo a los conceptos básicos de la programación: los tipos de datos más comunes, qué es HTML y JavaScript, cómo trabajar con variables, constantes, case-sensitive, entre otros.
  - Sabes documentar bloques de código para explicar la tarea que realizan, qué parámetros reciben y qué valor van a retornar.
  - Sigues las características esenciales para documentar código de muestra: el código debe ser correcto, conciso, comprensible, comentado, reutilizable y secuenciado.
  - Aprendiste a organizar documentos de una manera coherente a través de esquemas, escribiendo introducciones enganchadoras, añadiendo navegación en tu documento y revelando información paulatinamente.
  - Sabes cómo crear ilustraciones simples, instructivas, legibles a la vista, teniendo en cuenta el pie de imagen y usando señales visuales para que tu lector logre comprender nuestro contenido.

💚 ¡Qué genial que domines todas estas habilidades! Recuerda que tu camino no termina aquí. Nunca parar de aprender es esencial para trabajar en el mundo digital.