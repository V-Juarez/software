#include <iostream>

using namespace std;

int main() {
  bool resultado = true && true;
  cout << resultado;

  // or
  bool resultado1 = true || true;
  cout << resultado1;

  // not 
  bool resultado2 = !true;
  cout << resultado2;

  int edad = 0;
  cout << "edad: ";
  cin >> edad;
  // if (edad >= 18) {
  //   if (edad <= 40) {
  //     cout << "Puedes votar";
  //   } else  {
  //     cout << "No puedes votar";
  //   }
  // } else {
  //   cout << "No puedes votar";
  // }
  if (edad < 18) {
    cout << "No puedes votar";
  } else if (edad > 40) {
    cout << "No puedes Votar";
  } else {
    cout << "Puedes Votar";
  }
  return 0;
}