#include <iostream>

using namespace std;

int suma(int a, int b) {
  return a + b;
}

int multiplicacion(int c, int d = 2) {
  return c * d;
}

int main() {
  cout << suma(2, 2) << endl;

  cout << multiplicacion(4, 8) << endl;
}