#include <iostream>
#include <string>

using namespace std;

int main() {
  string text = "Hello World";

  cout << text << endl;
  cout << text.size() << endl;

  // int
  string text1 = "10";
  cout << stoi(text1) << endl;

  // float
  string text2 = "10.8";
  cout << stof(text2) << endl;
}